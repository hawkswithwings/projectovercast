**NAME**
Laura
**DIALOGUE**
Ace! Did you forget? Today’s the harvest festival! Everyone in town is at the farmers market, hurry up so you can join in the fun!
**NAME**
Ace
**DIALOGUE**
Alright, alright, I just finished up. I’m gonna make some lunch and I’ll meet you there.
**NAME**
Laura
**DIALOGUE**
Sounds good! I have to round up Wyatt so we’ll see you there!