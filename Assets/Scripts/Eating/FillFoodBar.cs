﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Yarn.Unity;

public class FillFoodBar : MonoBehaviour
{
    public PlayableDirector director;
    private ModifiedDialogueUI dialogueUI;
    private DialogueRunner runner;
    private PlayerMovement player;
    private VegeMeter vegeMeter;
    private SceneChangeManager sceneChangeManager;

    private bool fillMeter;
    private bool started = false;

    // Start is called before the first frame update
    void InitializeValues()
    {
        started = true;
        fillMeter = true;
        dialogueUI = FindObjectOfType<DialogueRunner>().gameObject.GetComponent<ModifiedDialogueUI>();
        runner = FindObjectOfType<DialogueRunner>();
        player = FindObjectOfType<PlayerMovement>().gameObject.GetComponent<PlayerMovement>();
        vegeMeter = FindObjectOfType<VegeMeter>().gameObject.GetComponent<VegeMeter>();
        sceneChangeManager = FindObjectOfType<SceneChangeManager>().gameObject.GetComponent<SceneChangeManager>();
    }

    private void OnEnable()
    {
        if (started == false)
            InitializeValues();

        if (fillMeter)
        {
            director.playableGraph.GetRootPlayable(0).Pause();
            List<string> vegetables = new List<string>(runner.gameObject.GetComponent<YarnWithInventory>().GetVegetables());
            runner.gameObject.GetComponent<YarnWithInventory>().ClearVegetables();
            vegeMeter.EatMeal(vegetables);
            fillMeter = false;
            StartCoroutine(resumeCutscene());
        }
        else
        {
            sceneChangeManager.ReturnToScene();
            // End the cut scene and stop eating
        }
    }

    // Update is called once per frame
    void Update()
    {
        player.enabled = false;
    }

    private IEnumerator resumeCutscene()
    {
        yield return new WaitUntil(() => vegeMeter.IsStillMoving() == false);
        director.playableGraph.GetRootPlayable(0).Play();
    }
}
