﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkingToYourself : MonoBehaviour
{
    public string playerName;
    public string textPath;

    private List<string> statements;
    private List<string> names;
    public DialogueBox textBox;

    private bool talking;

    private int textFile;

    private int index;

    public CutsceneConvoStart cutsceneConvoStart;

    // Start is called before the first frame update
    void Start()
    {
        //statements = new List<string>();
        //textFile = 1;
        /*string[] dialogue = System.IO.File.ReadAllLines(textPath + textFile + ".txt");
        for (int i = 0; i < dialogue.Length; i++)
        {
            if(dialogue[i].Equals("**DIALOGUE**"))
            {
                statements.Add(dialogue[i + 1]);
            }
        }
        index = 0;*/

        //StartCoroutine(DisplayStatement());
        //talking = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            NextLine();
        }
    }

    public void UpdateTextFile(string filePath)
    {
        statements = new List<string>();
        names = new List<string>();
        string[] dialogue = System.IO.File.ReadAllLines(textPath + filePath + ".txt");
        for (int i = 0; i < dialogue.Length; i++)
        {
            if (dialogue[i].Equals("**NAME**"))
                names.Add(dialogue[i + 1]);
            if (dialogue[i].Equals("**DIALOGUE**"))
                statements.Add(dialogue[i + 1]);
            
        }
        index = 0;
    }

    public void StartTalking()
    {
        StartCoroutine(DisplayStatement());
        talking = true;
    }

    public IEnumerator DisplayStatement()
    {
        textBox.SetName(playerName);
        textBox.ShowTextBox();
        textBox.SetName(names[index]);
        textBox.SetTextDisplay(statements[index]);
        yield return new WaitUntil(() => textBox.transform.localScale.x >= 1.0f);
        textBox.StartTalking();
    }

    public IEnumerator HideStatement()
    {
        textBox.HideTextBox();
        yield return new WaitUntil(() => textBox.transform.localScale.x <= 0.0f);
        textBox.RemoveName();

        textFile++;

        if(cutsceneConvoStart != null)
        {
            cutsceneConvoStart.ResumeCutscene();
            cutsceneConvoStart = null;
        }
        else
            GetComponent<PlayerMovement>().ActivateMovement();
        this.enabled = false;
    }

    public void NextLine()
    {
        if (textBox.transform.localScale.x < 1.0f)
        {
            //StartCoroutine(DisplayStatement());
            talking = true;

        }
        else if (talking)
        {
            talking = textBox.DisplayNextLine();
            //true if there's more text to display, false otherwise

        }
        if (!talking && index < statements.Count - 1)
        {
            index++;
            textBox.SetName(names[index]);
            textBox.SetTextDisplay(statements[index]);
            textBox.StartTalking();
            talking = true;
        }
        else if (textBox.transform.localScale.x >= 1.0f && !talking && index >= statements.Count - 1)
        {
            StartCoroutine(HideStatement());
        }

    }
}
