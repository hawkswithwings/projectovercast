﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SavingAndLoading : MonoBehaviour
{

    private List<Save> saves;

    public GameObject player;
    public List<GameObject> npcs;

    private string saveFilePath;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        saves = new List<Save>();
        saveFilePath = Application.persistentDataPath + "/gamesave.save";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SaveGame();
        }

        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            LoadGame();
        }
    }

    private Save CreateSave()
    {
        Save save = new Save();
        save.playerX = player.transform.position.x;
        save.playerY = player.transform.position.y;

        foreach(GameObject npc in npcs)
        {
            save.npcX.Add(npc.transform.position.x);
            save.npcY.Add(npc.transform.position.y);
            save.travelPoint.Add(npc.GetComponent<TalkativeNPC>().GetTravelIndex());
            save.npcDirection.Add(npc.GetComponent<TalkativeNPC>().GetDirection());
        }

        return save;
    }

    public void SaveGame()
    {
        Save save = CreateSave();

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(saveFilePath);
        bf.Serialize(file, save);
        file.Close();

    }

    public void LoadGame()
    {
        if(File.Exists(saveFilePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(saveFilePath, FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            file.Close();

            Vector3 playerPos = new Vector3(save.playerX, save.playerY, 0.0f);

            player.transform.position = playerPos;
            for(int i = 0; i < npcs.Count; i++)
            {
                Vector3 npcPos = new Vector3(save.npcX[i], save.npcY[i], 0.0f);

                npcs[i].transform.position = npcPos;
                npcs[i].GetComponent<TalkativeNPC>().SetTravelIndex(save.travelPoint[i]);
                npcs[i].GetComponent<TalkativeNPC>().SetDirection(save.npcDirection[i]);
            }

        }
    }
}
