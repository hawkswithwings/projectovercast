﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameExiter : MonoBehaviour
{
    private float buttonTimer;
    // Start is called before the first frame update
    void Start()
    {
        buttonTimer = 0.0f;
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Cancel"))
        {
            buttonTimer += Time.deltaTime;
            if (buttonTimer >= 3.0f)
                Application.Quit();
        }
        if (Input.GetButtonUp("Cancel"))
            buttonTimer = 0.0f;
    }
}
