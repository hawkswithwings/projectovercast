﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class Inventory : TextBoxActions
{
    class RestoreSlot
    {
        public InventorySlot inventorySlot;
        public int row;
        public int col;

        public RestoreSlot(InventorySlot inventorySlot, int row, int col)
        {
            this.inventorySlot = inventorySlot;
            this.row = row;
            this.col = col;
        }
    }

    public DialogueRunner runner;
    private YarnVariableStorage variableStorage;
    private ModifiedDialogueUI dialogueUI;
    private string selectedVeg;

    public GameObject[] tiles;
    private GameObject[,] OrderedTiles;
    private GameObject[,] Vegetables, Locations, People; //These will be the various "pages" that you click through
    private bool canClose;
    private List<RestoreSlot> restoreSlots;

    public Sprite[] Images;

    /* 
       We need to keep track of where in the OrderedTiles list we havet certain items with this dictionary
       string = item name 
       int[] = x, y in OrderedTiles
    */

    Dictionary<string, int[]> FillSlots = new Dictionary<string, int[]>();

    // curRow/curCol = currently looking at position
    // curFillRow/curFillCol = lastmost position to be filled

    public int curRow, curCol, curFillRow, curFillCol;

    //timer and PauseLimit to space out the actual input reading
    public float timer = 0, PauseLimit;

    public PlayerMovement player;
    private InventoryInteraction plotInspection;

    public Text Descriptor;

    public Color selected, unselected;

    public DialogueOptions yesOrNo;
    public DialogueBox question;
    private bool canTraverse;
    private string plant;
    private bool askUser;
    private bool removeItem;
    
    // Start is called before the first frame update
    public override void Start()
    {
        //base.Start();
        restoreSlots = new List<RestoreSlot>();

        //tiles = GameObject.FindGameObjectsWithTag("InvTile");

        
        //slots = GetComponentsInChildren<InventorySlot>();

        int rows = tiles.Length / 5;
        int cols = tiles.Length / 4;
        OrderedTiles = new GameObject[rows, cols];
        int row = 0; 
        int col = 0;
        int rowSize = OrderedTiles.GetLength(1);

        //This for loop sets up the current list of tiles as a 2D array of tiles
        for (int x = 0; x < tiles.Length; x++)
        {
            OrderedTiles[row, col] = tiles[x];
            col++;
            if (col == rowSize)
            {
                row++;
                col = 0;
            }
        }

        //open = false;

        curRow = 0;
        curCol = 0;
        canClose = true;

        OrderedTiles[curRow, curCol].GetComponent<Image>().color = selected;
        Descriptor.text = "Description: \n" + OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().GetItemDescrip();

        StarterSet();

        canTraverse = true;
        plant = "Do you want to plant ";

        plotInspection = player.gameObject.GetComponent<InventoryInteraction>();
        askUser = true;
        removeItem = false;

        runner = FindObjectOfType<DialogueRunner>();
        variableStorage = (YarnVariableStorage)runner.variableStorage;
        dialogueUI = runner.gameObject.GetComponent<ModifiedDialogueUI>();
        selectedVeg = "";
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (transform.localScale.x >= 1.0f)
        {
            if (timer <= 0)
            {
                if (canTraverse)
                {
                    TraverseInv();

                    if (Input.GetButtonDown("Fire2") && canClose && transform.localScale.x > 0.0f)
                    {
                        StartCoroutine(HideBackpack());
                    }
                }

                if (Input.GetButtonDown("Fire1") && OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().HasItem())
                {
                    // Ask whether the user wants to plant the items, and how many they want to plant, if not near plot, do nothing. 
                    // If we use the amount the Inventory Slot should return 1, 0 if the amount is too high
                    // If the slot is emptied (all of the amount is used), we return 2.
                    canTraverse = false;
                    ProcessItemClickYarn();
                    //ProcessItemClick();
                }
            }

            else if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
          
        }
    }


    //This method is called on to actually move along the inventory in four directions
    private void TraverseInv()
    {
        // First, we deselect the current tile
        

        //Read the input on H and V...
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        if(h != 0 || v != 0)
        {
            OrderedTiles[curRow, curCol].GetComponent<Image>().color = unselected;

            curCol += (int)h;
            // Go to to first column if we go over
            if(curCol > OrderedTiles.GetLength(1) - 1)
            {
                curCol = 0;
            }
            // Go to the last column if we go too far
            else if(curCol < 0)
            {
                curCol = OrderedTiles.GetLength(1) - 1;
            }
            timer = PauseLimit;

            if (v < 0)
            {
                curRow++;
                if (curRow > OrderedTiles.GetLength(0) - 1)
                {
                    curRow = 0;
                }
                timer = PauseLimit;
            }
            else if (v > 0)
            {
                curRow--;
                if (curRow < 0)
                {
                    curRow = OrderedTiles.GetLength(0) - 1;
                }
                timer = PauseLimit;
            }

            OrderedTiles[curRow, curCol].GetComponent<Image>().color = selected;

            Descriptor.text = "Description: \n" + OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().GetItemDescrip();
        }


    }


    private IEnumerator HideBackpack()
    {
        HideTextBox();
        yield return new WaitUntil(() => transform.localScale.x <= 0.0f);
        Debug.Log("Inventory's HideBackpack activating movement");
        player.ActivateMovement();
        plotInspection.StopWaiting(); 
        if (canClose == false)
            canClose = true;
    }

    public bool AddItem(InventoryItem newItem)
    {
        // First check if the key exists in the FilledSlots dictionary

        if (FillSlots.ContainsKey(newItem.GetItemName()))
        {
            int row = FillSlots[newItem.GetItemName()][0];
            int col = FillSlots[newItem.GetItemName()][1];

            OrderedTiles[row, col].GetComponent<InventorySlot>().AddItem(newItem);

            return true; 
        }

        else
        {
            // Complicated line: check the latest position, add the item to the inventory slot
            OrderedTiles[curFillRow, curFillCol].GetComponent<InventorySlot>().AddItem(newItem);

            int[] member = { curFillRow, curFillCol };
            // Add the newly added item to the FilledSlots dictionary in case we get more
            FillSlots.Add(newItem.GetItemName(), member);
            curFillCol++;

            if (curFillCol > OrderedTiles.GetLength(1) - 1)
            {
                curFillCol = 0;
                curFillRow++;
            }
            if (curFillRow > OrderedTiles.GetLength(0) - 1)
            {
                //cannot add if we have no room
                return false;
            }
        }
        return false;
    }

    private void StarterSet()
    {
        /*
        tiles[0].GetComponent<InventorySlot>().AddItem(new InventoryItem("Turnip", "It's a Turnip. I guess you can eat it.", Images[0], 2));
        tiles[1].GetComponent<InventorySlot>().AddItem(new InventoryItem("Carrot", "It's a Carrot. Good for your eyes.", Images[1], 2));
        tiles[2].GetComponent<InventorySlot>().AddItem(new InventoryItem("Beet", "It's a Beet. People who eat these gotta be courageous!", Images[2], 2));
        tiles[3].GetComponent<InventorySlot>().AddItem(new InventoryItem("Asparagus", "It's a bundle of Asparagus. They make your nose feel funny...", Images[3], 2));
        tiles[4].GetComponent<InventorySlot>().AddItem(new InventoryItem("Cauliflower", "It's bleached Broccoli! Wait, no, it's Cauliflower. These have a rough texture...", Images[4], 2));
        tiles[5].GetComponent<InventorySlot>().AddItem(new InventoryItem("Onion", "It's an Onion. These taste great, just listen to them sizzle in oil.", Images[5], 2));
        curFillCol = 1;
        curFillRow = 1;*/

        AddItem(new InventoryItem("Turnip", "It's a Turnip. I guess you can eat it.", Images[0], 2));
        AddItem(new InventoryItem("Carrot", "It's a Carrot. Good for your eyes.", Images[1], 2));
        AddItem(new InventoryItem("Beet", "It's a Beet. People who eat these gotta be courageous!", Images[2], 2));
        AddItem(new InventoryItem("Asparagus", "It's a bundle of Asparagus. They make your nose feel funny...", Images[3], 2));
        AddItem(new InventoryItem("Cauliflower", "It's bleached Broccoli! Wait, no, it's Cauliflower. These have a rough texture...", Images[4], 2));
        AddItem(new InventoryItem("Onion", "It's an Onion. These taste great, just listen to them sizzle in oil.", Images[5], 2));


    }

    public Sprite GetVegetableSprite(string name)
    {
        int index = 0;
        switch (name)
        {
            case "Turnip":
                index = 0;
                break;
            case "Carrot":
                index = 1;
                break;
            case "Beet":
                index = 2;
                break;
            case "Asparagus":
                index = 3;
                break;
            case "Cauliflower":
                index = 4;
                break;
            case "Onion":
                index = 5;
                break;
            default:
                index = 0;
                break;
        }

        return Images[index];
    }

    public void UseItem(string ItemName, int UsedAmount, bool rememberItem = false)
    {
        //The actual using of the items
        for (int row = 0; row < OrderedTiles.GetLength(0); row++)
        {
            for(int col = 0; col < OrderedTiles.GetLength(1); col++)
            {
                InventorySlot iSlot = OrderedTiles[row, col].GetComponent<InventorySlot>();
                if(iSlot != null)
                {
                    if (iSlot.HasItem())
                    {
                        if (iSlot.GetItemName().Equals(ItemName))
                        {
                            if (rememberItem)
                            {
                                InventorySlot tempSlot = new InventorySlot();
                                tempSlot.copySlot(iSlot);
                                RestoreSlot restoreSlot = new RestoreSlot(tempSlot, row, col);
                                restoreSlots.Add(restoreSlot);
                            }
                            iSlot.UseItem(UsedAmount);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void RestoreSlots()
    {
        for(int i = 0; i < restoreSlots.Count; i++)
        {
            OrderedTiles[restoreSlots[i].row, restoreSlots[i].col].GetComponent<InventorySlot>().copySlot(restoreSlots[i].inventorySlot);
        }
        restoreSlots.Clear();
    }

    private void ProcessItemClickYarn()
    {
        if(runner.IsDialogueRunning && string.IsNullOrEmpty(selectedVeg))
        {
            selectedVeg = OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().GetItemName(); 
            runner.gameObject.GetComponent<YarnWithInventory>().SetVegeSelected(selectedVeg);
        }
        else if(!string.IsNullOrEmpty(selectedVeg))
        {
            dialogueUI.MarkLineComplete();
        }
        else
        {
            canTraverse = true;
        }
    }

    [YarnCommand("updatePlot")]
    public void UpdatePlot()
    {
        string plantName = OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().GetItemName();
        string description = OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().GetItemDescrip();
        plotInspection.plot.PlantVegetable(plantName, description);
        OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().UseItem(1);
    }

    private void ProcessItemClick()
    {
        if (question.transform.localScale.x <= 0.0f)
        {
            StartCoroutine(DisplayOrHideBox(question));
        }
        else if(question.transform.localScale.x >= 1.0f && !question.DisplayNextLine())
        {
            //question.DisplayNextLine returning false means that there isn't anything left to display in the text box
            //So now we gotta display "yes" or "no"
            if (askUser)
            {
                if (yesOrNo.transform.localScale.x <= 0.0f)
                    StartCoroutine(DisplayOrHideBox(yesOrNo));
                else
                {
                    int selection = yesOrNo.GetSelectedOption();
                    if (selection == 0)
                    {
                        //Player chose yes, either plant it or say they cannot plant it if no plot selected.
                        string message = "You cannot plant vegetables here.";
                        askUser = false;
                        if (plotInspection.plot != null)
                        {
                            string plantName = OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().GetItemName();
                            string description = OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().GetItemDescrip();
                            string aOrAn = "a ";
                            if(plantName[0] == 'O' || plantName[0] == 'A')
                            {
                                aOrAn = "an ";
                            }
                            message = "You planted " + aOrAn + plantName + ".";
                            plotInspection.plot.PlantVegetable(plantName, description);
                            removeItem = true;
                        }
                        StartCoroutine(HideAndDisplayMessage(message));
                    }
                    else
                    {
                        //close windows.
                        yesOrNo.ResetCursor();
                        StartCoroutine(DisplayOrHideBox(yesOrNo));
                        StartCoroutine(DisplayOrHideBox(question));
                    }
                }
            }
            else
            {
                StartCoroutine(DisplayOrHideBox(question));
                if(plotInspection.plot != null)
                {
                    StartCoroutine(HideBackpack());
                    plotInspection.plot.StopWaitingForInventory();
                    if (removeItem)
                    {
                        OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().UseItem(1);
                        removeItem = false;
                    }
                }
            }
            
        }
    }

    private IEnumerator HideAndDisplayMessage(string message)
    {
        yesOrNo.ResetCursor();
        StartCoroutine(DisplayOrHideBox(yesOrNo));
        yield return new WaitUntil(() => yesOrNo.transform.localScale.x <= 0.0f); 
        question.SetTextDisplay(message);
        question.StartTalking();
    }

    private IEnumerator DisplayOrHideBox(TextBoxActions tba)
    {
        if(tba.transform.localScale.x < 1.0f)
        {
            //show
            tba.ShowTextBox();

            yield return new WaitUntil(() => tba.transform.localScale.x >= 1.0f);
            try
            {
                string plantName = OrderedTiles[curRow, curCol].GetComponent<InventorySlot>().GetItemName();
                ((DialogueBox)tba).SetTextDisplay(plant + "a " + plantName + "?");
                ((DialogueBox)tba).StartTalking();
                askUser = true;
            }
            catch (Exception e) { }

        }
        else
        {
            //hide
            tba.HideTextBox();
            yield return new WaitUntil(() => tba.transform.localScale.x <= 0.0f);
            canTraverse = true;
        }
    }

    public void ShowInventoryYarn()
    {
        selectedVeg = "";
        canClose = false;
        this.ShowTextBox();
    }

    public void AllowMovement()
    {
        selectedVeg = "";
        canClose = false;
        canTraverse = true;
    }

    public void HideInvetoryYarn()
    {
        canTraverse = true;
        canClose = true;
        this.HideTextBox();
    }

}
