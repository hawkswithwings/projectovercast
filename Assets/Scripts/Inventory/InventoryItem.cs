﻿using UnityEngine;

public class InventoryItem
{
    private string ItemName;
    private string ItemDescription;
    private Sprite ItemImage;
    private int ItemSupply;

    public InventoryItem()
    {
        ItemName = "";
        ItemDescription = "";
        ItemImage = null;
        ItemSupply = 0;
    }

    public InventoryItem(string name, string descrip, Sprite image, int supply)
    {
        ItemName = name;
        ItemDescription = descrip;
        ItemImage = image;
        ItemSupply = supply;
    }

    public void copyItem(InventoryItem item)
    {
        this.ItemName = item.ItemName;
        this.ItemDescription = item.ItemDescription;
        this.ItemImage = item.ItemImage;
        this.ItemSupply = item.ItemSupply;
    }

    public string GetItemName()
    {
        return ItemName;
    }
    public string GetDescription()
    {
        return ItemDescription;
    }
    public Sprite GetItemImage()
    {
        return ItemImage;
    }
    public int GetSupply()
    {
        return ItemSupply;
    }

    public void IncSupply(int increment)
    {
        ItemSupply += increment;
    } 
    public void DecSupply(int decrement)
    {
        ItemSupply -= decrement;
    }
    public void SetItemName(string newName)
    {
        ItemName = newName;
    }
    public void SetDescription(string newDescription)
    {
        ItemDescription = newDescription;
    }
    public void SetItemImage(Sprite newImage)
    {
        ItemImage = newImage;
    }

}
