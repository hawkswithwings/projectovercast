﻿
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    private InventoryItem item;
    public Image icon, amount;
    public Text amountT;

    public void AddItem(InventoryItem newItem)
    {
        if (item == null)
        {
            item = newItem;
            icon.sprite = item.GetItemImage();
            amount.gameObject.SetActive(true);
            UpdateSupply();
        }
        else if (item.GetItemName().Equals(newItem.GetItemName()))
        {
            item.IncSupply(newItem.GetSupply());
            UpdateSupply();
        }
    }

    public int UseItem(int amount)
    {
        if (amount > item.GetSupply())
        {
            //  return false means that the amount was too much.
            return 0;
        }
        else
        {
            item.DecSupply(amount);
            UpdateSupply();
            if (item.GetSupply() <= 0)
            { 
                //Clear out the slot, since the item is gone.
                ClearSlot();
                return 2;
            }
            return 1;
        }
        
    }

    public void copySlot(InventorySlot slot)
    {
        if (item == null)
            item = new InventoryItem();
        item.copyItem(slot.item);
        icon = slot.icon;
        amount = slot.amount;
        amountT = slot.amountT;
        icon.sprite = slot.item.GetItemImage();
        UpdateSupply();
    }

    public void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        UpdateSupply();
    }

    public string GetItemName()
    {
        if (item.GetItemName() != null)
            return item.GetItemName();
        else
            return "";
    }

    public string GetItemDescrip()
    {
        if (item != null)
            return item.GetDescription();
        else
            return "";
    }

    public bool HasItem()
    {
        if (item == null)
            return false;
        else
            return true;
    }

    private void UpdateSupply()
    {
        if (item != null)
        {
            amountT.text = item.GetSupply().ToString();
        }
        else
            amount.gameObject.SetActive(false);
    }
}
