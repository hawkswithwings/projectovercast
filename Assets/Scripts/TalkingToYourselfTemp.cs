﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class TalkingToYourselfTemp : MonoBehaviour
{
    public bool stayOn;
    public DialogueRunner runner;
    public YarnProgram scriptToLoad;
    public DialogueBox textBox;
    public string playerName;

    private bool talking;

    private int textFile;

    private int index;
    private ModifiedDialogueUI dialogueUI;

    public CutsceneConvoStart cutsceneConvoStart;

    private bool reactivateTalkToNPC;

    // Start is called before the first frame update
    void Start()
    {
        dialogueUI = runner.gameObject.GetComponent<ModifiedDialogueUI>();
        //runner.Add(scriptToLoad);
        this.enabled = stayOn;
        reactivateTalkToNPC = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            NextLine();
        }
    }

    public void UpdateTextFile(string filePath)
    {
        
    }

    public void UpdateYarnFile(YarnProgram newScript)
    {
        scriptToLoad = newScript;
        runner.Add(scriptToLoad);
    }

    public void StartTalking()
    {
        if (textBox.transform.localScale.x > 0.0f)
        {
            StartCoroutine(waitThenStartTalking());
        }
        else
        {
            dialogueUI.onDialogueEnd.AddListener(HideStatement);
            talking = true;
            runner.StartDialogue(scriptToLoad.name);
            //StartCoroutine(DisplayStatement());
        }
    }

    private IEnumerator waitThenStartTalking()
    {
        PlayerMovement playerMovement = GetComponent<PlayerMovement>();
        PlayerTalkToNPC talkToNPC = GetComponent<PlayerTalkToNPC>();
        reactivateTalkToNPC = talkToNPC.enabled;
        talkToNPC.enabled = false;
        playerMovement.enabled = false;
        yield return new WaitUntil(() => textBox.transform.localScale.x <= 0.0f);
        yield return new WaitForSecondsRealtime(1.0f);
        StartTalking();
    }

    public IEnumerator DisplayStatement()
    {
        textBox.SetName(playerName);
        textBox.ShowTextBox();
        yield return new WaitUntil(() => textBox.transform.localScale.x >= 1.0f);
        runner.StartDialogue(scriptToLoad.name);
    }

    public void HideStatement()
    {
        textBox.RemoveName();
        dialogueUI.onDialogueEnd.RemoveListener(HideStatement);
        if (reactivateTalkToNPC)
            GetComponent<PlayerTalkToNPC>().enabled = true;
        textFile++;

        if (cutsceneConvoStart != null)
        {
            cutsceneConvoStart.ResumeCutscene();
            cutsceneConvoStart = null;
        }
        else
        {
            Debug.Log("TalkingToYourselfTemp activating movement");
            GetComponent<PlayerMovement>().ActivateMovement();
        }
        this.enabled = false;
    }

    public void NextLine()
    {
        dialogueUI.MarkLineComplete();
    }
}
