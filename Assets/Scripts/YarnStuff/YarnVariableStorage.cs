﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn;

public class YarnVariableStorage : Yarn.Unity.VariableStorageBehaviour
{
    public Dictionary<string, Yarn.Value> variables = new Dictionary<string, Yarn.Value>();

    public override void SetValue(string variableName, Yarn.Value value)
    {
        // 'variableName' is the name of the variable that 'value' 
        // should be stored in.
        if (variables.ContainsKey(variableName))
            variables[variableName] = value;
        else
            variables.Add(variableName, value);

    }

    // Return a value, given a variable name
    public override Yarn.Value GetValue(string variableName)
    {
        // 'variableName' is the name of the variable to return a value for
        if (variables.ContainsKey(variableName))
            return variables[variableName];
        else
            return null;
    }

    // Return to the original state
    public override void ResetToDefaults()
    {
        foreach(string key in variables.Keys)
        {
            variables[key] = new Yarn.Value();
        }
    }
}
