﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class YarnWithInventory : MonoBehaviour
{
    public Inventory backpack;
    public GameObject mainTextBox;
    public DialogueOptions yesNoOptions;
    public DialogueOptions usualOptions;
    public List<Button> yesNo;

    private PlayerTalkToNPC talkToNPC;
    private InventoryInteraction invInteraction;
    private DialogueRunner runner;
    private YarnVariableStorage variableStorage;
    private ModifiedDialogueUI dialogueUI;
    private List<Button> originalOptions;
    private SceneChangeManager sceneChangeManager;
    private PlayerMovement playerMovement;

    private List<string> vegetables;

    private string vegeSelected;

    // Start is called before the first frame update
    void Start()
    {
        vegetables = new List<string>();
        runner = GetComponent<DialogueRunner>();
        variableStorage = (YarnVariableStorage)runner.variableStorage;
        dialogueUI = GetComponent<ModifiedDialogueUI>();

        originalOptions = new List<Button>();
        foreach (Button button in dialogueUI.optionButtons)
            originalOptions.Add(button);

        runner.AddCommandHandler(
            "displayBox",
            DisplayBox
        );

        runner.AddCommandHandler(
            "selectFromInventory",
            SelectFromInventory
        );
        vegeSelected = "";

        runner.AddCommandHandler(
            "changeOptions",
            ChangeOptions
        );

        runner.AddCommandHandler(
            "closeBackpack",
            CloseBackpack
        );


        runner.AddCommandHandler(
           "eatingThis",
           EatingThis
        );

        runner.AddCommandHandler(
            "restoreSelectedVeg", 
            RestoreSelectedVeg
        );

        runner.AddCommandHandler(
            "startEatingScene",
            EatingScene
        );

        runner.AddCommandHandler(
            "closeOptions", 
            CloseOptions
        );

        runner.AddCommandHandler(
            "stopUsingFridge",
            StopUsingFridge
        );

        talkToNPC = transform.parent.gameObject.GetComponent<PlayerTalkToNPC>();
        playerMovement = transform.parent.gameObject.GetComponent<PlayerMovement>();
        invInteraction = transform.parent.gameObject.GetComponent<InventoryInteraction>();
        sceneChangeManager = transform.parent.gameObject.GetComponent<SceneChangeManager>();
        if (usualOptions != null && talkToNPC != null)
        {
            dialogueUI.onOptionsStart.AddListener(usualOptions.ShowTextBox);
            dialogueUI.onOptionsStart.AddListener(talkToNPC.StartChoosing);
            dialogueUI.onOptionsEnd.AddListener(usualOptions.HideTextBox);
            dialogueUI.onDialogueEnd.AddListener(talkToNPC.CanNowMove);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EatingThis(string[] parameters)
    {
        string curVeg = parameters[0];
        vegetables.Add(curVeg);
        backpack.UseItem(curVeg, 1, true);
    }

    public void RestoreSelectedVeg(string[] parameters)
    {
        backpack.RestoreSlots();
    }

    public void EatingScene(string[] parameters)
    {
        playerMovement.toggleStayOff();
        dialogueUI.onDialogueEnd.AddListener(GoToEatingScene);
    }

    private void GoToEatingScene()
    {
        talkToNPC.enabled = false;
        dialogueUI.onDialogueEnd.RemoveListener(GoToEatingScene);
        Vector3 targetPos = new Vector3(-0.254367f, -0.3300016f);
        string returnScene = SceneManager.GetActiveScene().name;
        if (returnScene.Equals("InsidePlayerHouseIntro"))
            returnScene = "InsidePlayerHouse";
        sceneChangeManager.changeScene("Eating_InsidePlayerHouse", targetPos, returnScene);
    }

    public void SelectFromInventory(string[] parameters, System.Action onComplete)
    {
        StartCoroutine(WaitForInventory(onComplete));
    }

    private IEnumerator WaitForInventory(System.Action onComplete)
    {
        vegeSelected = "";
        mainTextBox.GetComponent<DialogueBox>().HideTextBox();
        if (backpack.transform.localScale.x > 0.0f == false)
            backpack.ShowInventoryYarn();
        else
            backpack.AllowMovement();
        yield return new WaitUntil(() => string.IsNullOrEmpty(vegeSelected) == false);
        mainTextBox.GetComponent<DialogueBox>().ShowTextBox();

        variableStorage.SetValue("$VegNameNoPunc", new Yarn.Value(vegeSelected));
        if ("aeiouAEIOU".IndexOf(vegeSelected[0]) >= 0)
            vegeSelected = "an " + vegeSelected;
        else
            vegeSelected = "a " + vegeSelected;

        variableStorage.SetValue("$VegName", new Yarn.Value(vegeSelected));
        vegeSelected = "";

        // Call the completion handler
        onComplete();

        // Yarn Spinner will now continue running!
    }

    public void CloseBackpack(string[] parameters)
    {
        //backpack.HideTextBox();
        backpack.HideInvetoryYarn();
    }

    public void DisplayBox(string[] parameters, System.Action onComplete)
    {
        StartCoroutine(WaitForDisplayBox(onComplete));
    }

    public void CloseOptions(string[] parameters)
    {
        if (yesNoOptions.transform.localScale.x > 0.0f)
            yesNoOptions.HideTextBox();
        if (usualOptions.transform.localScale.x > 0.0f)
            usualOptions.HideTextBox();
    }

    public void StopUsingFridge(string[] parameters)
    {
        bool allowMovement = true;
        if (parameters != null)
            allowMovement = false;
        invInteraction.StopUsingFridge(allowMovement);
    }

    private IEnumerator WaitForDisplayBox(System.Action onComplete)
    {
        yield return new WaitUntil(() => mainTextBox.transform.localScale.x >= 1.0f);
        onComplete();
    }

    public void ChangeOptions(string[] parameters)
    {
        dialogueUI.optionButtons.Clear();
        dialogueUI.onOptionsStart.RemoveAllListeners();
        dialogueUI.onOptionsEnd.RemoveAllListeners();
        List<Button> buttons = originalOptions;

        if (parameters[0].Equals("yesNo"))
        {
            Debug.Log("yesNo read as argument");
            buttons = yesNo;
            dialogueUI.onOptionsStart.AddListener(yesNoOptions.ShowTextBox);
            dialogueUI.onOptionsEnd.AddListener(yesNoOptions.HideTextBox);
        }
        else
        {
            yesNoOptions.HideTextBox();
            dialogueUI.onOptionsStart.AddListener(usualOptions.ShowTextBox);
            dialogueUI.onOptionsEnd.AddListener(usualOptions.HideTextBox);
        }

        if(parameters.Length > 1)
        {
            if (parameters[1].Equals("PlayerTalkToNPC"))
            {
                dialogueUI.onOptionsStart.AddListener(talkToNPC.StartChoosing);
                dialogueUI.onDialogueEnd.AddListener(talkToNPC.CanNowMove);
            }
            else if (parameters[1].Equals("InventoryInteraction"))
            {
                dialogueUI.onOptionsStart.AddListener(invInteraction.StartChoosing);
                dialogueUI.onDialogueEnd.AddListener(invInteraction.CanNowMove);
            }

            if (parameters.Length > 2)
            {
                if (parameters[2].Equals("usingFridge"))
                {
                    invInteraction.usingFridge = true;
                    talkToNPC.enabled = false;
                }
            }
            else
                invInteraction.usingFridge = false;
        }
        else
        {
            dialogueUI.onOptionsStart.AddListener(talkToNPC.StartChoosing);
            dialogueUI.onDialogueEnd.AddListener(talkToNPC.CanNowMove);
        }

        foreach (Button button in buttons)
            dialogueUI.optionButtons.Add(button);
    }

    public void SetVegeSelected(string vegName)
    {
        vegeSelected = vegName;
    }

    public List<string> GetVegetables()
    {
        return vegetables;
    }
    public void ClearVegetables()
    {
        vegetables = new List<string>();
    }
}
