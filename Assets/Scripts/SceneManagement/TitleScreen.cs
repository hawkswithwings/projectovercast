﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour
{
    public string sceneToLoad;
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown && !Input.GetButtonDown("Cancel") && animator.GetBool("started") == false)
        {
            animator.SetBool("started", true);
            StartCoroutine(StartGame());
        }
    }

    private IEnumerator StartGame()
    {
        AsyncOperation sceneLoad = SceneManager.LoadSceneAsync("InsidePlayerHouseIntro");
        sceneLoad.allowSceneActivation = false; 
        yield return new WaitForSecondsRealtime(1.0f);
        while(sceneLoad.progress < 0.9f)
        {
            yield return null;
        }
        sceneLoad.allowSceneActivation = true;
    }
}
