﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneChangeFade : MonoBehaviour
{
    Image image;
    bool fade, show;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (fade)
        {
            Color newColor = image.color;
            newColor.a -= 0.01f;
            image.color = newColor;
            if (image.color.a <= 0.0f)
                fade = false;

        }

        else if (show)
        {
            Color newColor = image.color;
            newColor.a += 0.01f;
            image.color = newColor;
            if (image.color.a >= 1.0f)
                show = false;
        }*/
    }

    public void Fade()
    {
        fade = true;
        animator.Play("FadeIn");
    }

    public bool isFading()
    {
        return fade;
    }

    public void Show()
    {
        show = true;
        animator.Play("FadeOut");
    }

    public bool isShowing()
    {
        return show;
    }

    public void FadeFinished()
    {
        fade = false;
    }

    public void ShowFinished()
    {
        show = false;
    }
}
