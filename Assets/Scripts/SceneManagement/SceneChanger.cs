﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    public string targetScene;
    public int spawnDirection;
    public Vector3 loadSpot; //This is the x and y of the target spawn point

    private float vDistance = 1.22f;
    private float hDistance = 1.0f;

    private void Start()
    {

        //0 up, 1 right, 2 down, 3 left
        switch (spawnDirection)
        {
            case 0:
                loadSpot.y += vDistance;
                break;
            case 1:
                loadSpot.x += hDistance;
                break;
            case 2:
                loadSpot.y -= vDistance;
                break;
            case 3:
                loadSpot.x -= hDistance;
                break;
        }
    }
}
