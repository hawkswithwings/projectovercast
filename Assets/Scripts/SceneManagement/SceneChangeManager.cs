﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Yarn.Unity;

public class SceneChangeManager : MonoBehaviour
{
    public GameObject mainCamera;
    public GameObject cmFocus;
    public SceneChangeFade blackFade;

    private bool stopSceneChange;

    private PlayerMovement playerMovement;
    private TalkingToYourselfTemp playerTalk;
    private PlayerTalkToNPC talkToNPC;
    private Rigidbody2D rb;

    private string sceneLoading;
    private Vector3 loadSpot;

    private bool triggerCutscene;
    private string cutsceneScene;

    private ScenarioManager scenarioManager;
    private List<GameObject> cutsceneCameras;
    private GameObject playerPos;

    private bool letCutscenePlay;

    private string returnScene;
    private Vector3 returnPos;

    // Start is called before the first frame update
    void Start()
    {
        triggerCutscene = true;
        if (GameObject.FindGameObjectsWithTag("Player").Length > 1)
        {
            Destroy(this.gameObject);
        }

        stopSceneChange = false;

        playerMovement = GetComponent<PlayerMovement>();
        playerTalk = GetComponent<TalkingToYourselfTemp>();
        talkToNPC = GetComponent<PlayerTalkToNPC>();
        rb = GetComponent<Rigidbody2D>(); 
        sceneLoading = "";
        loadSpot = new Vector3();

        BlackFade(true);

        scenarioManager = GetComponent<ScenarioManager>();
        cutsceneCameras = new List<GameObject>();
        playerPos = null;
        letCutscenePlay = false;

        DontDestroyOnLoadManager.DontDestroyOnLoad(this.gameObject);
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReturnToScene()
    {
        playerMovement.toggleStayOff();
        changeScene(returnScene, returnPos);
    }

    public void changeScene(string targetScene, Vector3 loadSpot, string returnScene = "")
    {
        if (string.IsNullOrEmpty(returnScene) == false)
        {
            this.returnScene = returnScene;
            returnPos = transform.position;
        }

        sceneLoading = targetScene;
        this.loadSpot = loadSpot;
        BlackFade(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("SceneChange"))
        {
            playerMovement.enabled = false;
            rb.velocity = Vector3.zero;
            if (stopSceneChange)
            {
                playerTalk.UpdateTextFile("StopAce");
                playerTalk.enabled = true;
                playerTalk.StartTalking();
            }
            else
            {
                SceneChanger sceneChanger = collision.gameObject.GetComponent<SceneChanger>();
                changeScene(sceneChanger.targetScene, sceneChanger.loadSpot);
            }
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        transform.position = loadSpot;
        loadSpot = Vector3.zero;
        sceneLoading = "";
        //if(GameObject.FindGameObjectWithTag("DialogueOptions") != null)
        //playerMovement.dOptions = GameObject.FindGameObjectWithTag("DialogueOptions").GetComponent<DialogueOptions>();

        GameObject timelineManager = GameObject.FindGameObjectWithTag("TimelineHandler");
        bool fadeIn = true;
        if(timelineManager != null)
        {
            if (timelineManager.GetComponent<TimelineFiller>() != null)
                fadeIn = false;
        }
        if(fadeIn)
            BlackFade(true);
        
        if (SceneManager.GetActiveScene().name.Equals("Farmland"))
            GetComponent<PlotInfo>().LoadPlotInfo();

        stopSceneChange = scenarioManager.shouldStopPlayer();

        playerPos = GameObject.FindGameObjectWithTag("PlayerPos");
        if (playerPos != null)
        {
            if(playerPos.transform.parent == playerMovement.transform)
            {
                Destroy(playerPos);
                playerPos = null;
            }
            else
            {
                playerPos.transform.parent = playerMovement.transform;
                playerPos.transform.localPosition = Vector2.zero;
            }
        }

        GameObject cutsceneManager = GameObject.FindGameObjectWithTag("CutsceneManager");
        if(cutsceneManager != null)
        {
            CutsceneAnimationHandler cah = cutsceneManager.GetComponent<CutsceneAnimationHandler>();
            cah.pAnimator = playerMovement.gameObject.GetComponent<Animator>();
            cutsceneManager.transform.GetChild(2).GetComponent<CutsceneConvoStart>().convo = playerTalk;
        }
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private IEnumerator DisplayBlackFade()
    {
        //blackFade.ShowTextBox();
        blackFade.Show();
        //yield return new WaitUntil(() => blackFade.transform.localScale.x >= 1.0f);
        yield return new WaitUntil(() => blackFade.isShowing() == false);
        SceneManager.LoadScene(sceneLoading);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private IEnumerator HideBlackFade()
    {
        SpriteRenderer playerSprite = GetComponent<SpriteRenderer>();
        if (letCutscenePlay)
        {
            //playerSprite.enabled = false;
            mainCamera.SetActive(false);
            cmFocus.SetActive(false);
        }
        else if (playerSprite.enabled == false)
        {
            playerSprite.enabled = true;
            mainCamera.SetActive(true);
            cmFocus.SetActive(true);
        }
        talkToNPC.cutTalkShort();
        yield return new WaitForSeconds(0.5f);
        //blackFade.HideTextBox();
        blackFade.Fade();
        //yield return new WaitUntil(() => blackFade.transform.localScale.x <= 0.0f);
        yield return new WaitUntil(() => blackFade.isFading() == false);
        startCutscene();
        scenarioManager.loadDialogue(SceneManager.GetActiveScene().name);
    }

    private void BlackFade(bool fade)
    {
        //TextBoxActions blackFade = GameObject.FindGameObjectWithTag("FadeToBlack").GetComponent<TextBoxActions>();
        //SceneChangeFade blackFade = GameObject.FindGameObjectWithTag("FadeToBlack").GetComponent<SceneChangeFade>();
        if (fade)
        {
            StartCoroutine(HideBlackFade());
            //playerMovement.enabled = true;
        }
        else
        {
            StartCoroutine(DisplayBlackFade());
        }
    }

    public void ShouldReadyCutscene(bool yesOrNo)
    {
        triggerCutscene = yesOrNo;
    }

    public void readyCutscene(string sceneName, bool playOnLoad)
    {
        triggerCutscene = playOnLoad;
        cutsceneScene = sceneName;

        if (sceneName.Equals(SceneManager.GetActiveScene().name))
        {
            startCutscene();
        }

        if (stopSceneChange) //only change the stopSceneChange if it is true when scenarios change
            stopSceneChange = scenarioManager.shouldStopPlayer();
    }

    private void startCutscene()
    {
        if (triggerCutscene)
        {
            GameObject cutsceneManager = GameObject.FindGameObjectWithTag("CutsceneManager");
            if (cutsceneManager != null)
            {
                CutsceneAnimationHandler cah = cutsceneManager.GetComponent<CutsceneAnimationHandler>();
                if (cah.enabled == false)
                {
                    cah.enabled = true;
                    cah.playCutscene();
                }
            }

            playerMovement.enabled = false;
            YarnProgram currentSkitFile = scenarioManager.GetCurrentSkitFile();
            if(currentSkitFile != null)
            {
                playerTalk.UpdateYarnFile(currentSkitFile);
                playerTalk.enabled = true;
                if (cutsceneManager == null)
                    playerTalk.StartTalking();
            }
        }
        else
        {
            Debug.Log("SceneChangeManager's startCutscene activating movement");
            playerMovement.ActivateMovement();
        }
    }

    [YarnCommand("toggleSceneChange")]
    public void ToggleSceneChange()
    {
        stopSceneChange = !stopSceneChange;
    }

    [YarnCommand("loadCutscene")]
    public void loadCutscene(string sceneName)
    {
        //This method will be used to fade to black then load a cutscene
        playerMovement.enabled = false;
        rb.velocity = Vector3.zero;
        sceneLoading = sceneName;
        loadSpot = gameObject.transform.position;
        letCutscenePlay = true;
        talkToNPC.cutTalkShort();
        BlackFade(false);
    }
}
                    