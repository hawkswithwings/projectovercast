﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class ScenarioManager : MonoBehaviour
{
    //This should be attached to the player and work in conjunction with SceneChangeManager
    //List of Scenarios that have, at least, a number and a name
    //Or a Dictionary of names as keys and numbers as values
    List<Scenario> scenarios; //List right now, but should probably only be one Scenario that changes as we advance
    private int currentScenario;

    public DialogueRunner runner;
    public Dictionary<string, int> scenarioFiles;

    public List<YarnProgram> yarnScripts;
    private int currentScript;

    private SceneChangeManager sceneChanger;

    private List<string> talkedTo;

    private bool startCutscene;

    // Start is called before the first frame update
    void Start()
    {
        currentScript = 0;
        scenarios = new List<Scenario>();
        scenarioFiles = new Dictionary<string, int>();

        AddInsideHouseTutorial();
        AddOutsideHouseTutorial();
        AddOutsideHouseLaura();
        AddMarketInteractions();
        AddAceTalk();
        currentScenario = 0;

        sceneChanger = GetComponent<SceneChangeManager>();
        talkedTo = new List<string>();

        startCutscene = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!runner.IsDialogueRunning && startCutscene)
        {
            startCutscene = false;
            updateSceneChanger();
        }
    }

    private void AddInsideHouseTutorial()
    {
        Scenario insideHouse = new Scenario();
        Skit wakeUp = new Skit(yarnScripts[currentScript++], "InsidePlayerHouse", true);
        insideHouse.mainSkits.Add(wakeUp);
        scenarios.Add(insideHouse);
    }

    private void AddOutsideHouseTutorial()
    {
        Scenario outsideHouse = new Scenario();
        outsideHouse.interactCount = 3;
        Skit firstPlot = new Skit(yarnScripts[currentScript], "Farmland", false, "MPlot");
        currentScript++;
        Skit secondPlot = new Skit(yarnScripts[currentScript], "Farmland", false, "TPlot");
        currentScript++;
        Skit thirdPlot = new Skit(yarnScripts[currentScript], "Farmland", false, "BPlot");
        currentScript++;
        outsideHouse.stopSceneChange = true;

        outsideHouse.mainSkits.Add(firstPlot);
        outsideHouse.mainSkits.Add(secondPlot);
        outsideHouse.mainSkits.Add(thirdPlot);
        scenarios.Add(outsideHouse);
    }

    private void AddOutsideHouseLaura()
    {
        Scenario lauraTalk = new Scenario();
        Skit firstSkit = new Skit(yarnScripts[currentScript], "Farmland", true);
        currentScript++;

        lauraTalk.mainSkits.Add(firstSkit);
        scenarios.Add(lauraTalk);
    }

    private void AddMarketInteractions()
    {
        Scenario market = new Scenario();
        market.interactCount = 5;
        scenarios.Add(market);
    }
    private void AddAceTalk()
    {
        Scenario ending = new Scenario();
        Skit firstSkit = new Skit(yarnScripts[currentScript], "Market", true);
        currentScript++;

        ending.mainSkits.Add(firstSkit);
        scenarios.Add(ending);
    }

    public YarnProgram GetCurrentSkitFile()
    {
        YarnProgram skitFile = null;
        if(scenarios[currentScenario].mainSkits.Count > 0)
            skitFile = scenarios[currentScenario].getCurrentSkit().GetSkitFile();
        MoveToNextSkit();
        return skitFile;
    }

    private void MoveToNextSkit()
    {
        if (scenarios[currentScenario].currentSkit < scenarios[currentScenario].mainSkits.Count - 1)
            scenarios[currentScenario].currentSkit++;
        else
        {
            currentScenario++;
            talkedTo.Clear();
        }

        if(currentScenario < scenarios.Count)
        {
            updateSceneChanger();
        }
    }

    private void updateSceneChanger()
    {
        if (scenarios[currentScenario].mainSkits.Count == 0)
        {
            sceneChanger.readyCutscene("", false);
            return;
        }
        Skit nextSkit = scenarios[currentScenario].getCurrentSkit();
        sceneChanger.readyCutscene(nextSkit.scene, nextSkit.playOnSceneLoad);
    }

    public void loadDialogue(string sceneName)
    {
        List<Skit> skits = scenarios[currentScenario].mainSkits;

        for(int i = 0; i < skits.Count; i++)
        {
            if (skits[i].scene.Equals(sceneName))
            {
                GameObject interact = GameObject.Find(skits[i].interactLoad);
                interact.GetComponent<TalkativeNPC>().UpdateFile(skits[i].GetSkitFile());
                interact.GetComponent<TalkativeNPC>().enabled = true;
            }
        }
    }

    [YarnCommand("updateInteracted")]
    public void updateInteracted()
    {
        startCutscene = scenarios[currentScenario].incrementNum();
        if(startCutscene)
            currentScenario++;
    }

    public bool shouldStopPlayer()
    {
        return scenarios[currentScenario].stopSceneChange;
    }
}

public class Scenario {
    //list of mainSkits in order
    public List<Skit> mainSkits = new List<Skit>();
    Dictionary<int, List<Skit>> sideSkits = new Dictionary<int,List<Skit>>();

    public int currentSkit;
    int currentSideSkit;

    public int interactCount;
    private int numInteracted = 0;

    public bool stopSceneChange = false;

    public Skit getCurrentSkit()
    {
        return mainSkits[currentSkit];
    }

    public bool incrementNum()
    {
        numInteracted++;
        if (numInteracted >= interactCount)
            return true;
        return false;
    }
}

public class Skit
{

    YarnProgram skitFileKey; //File that contains skit dialogue
    public string scene; //name of scene this plays in
    public bool playOnSceneLoad; //true if play on scene loaded
    public string interactLoad; //should be the name of the game object the player has to interact with to trigger this skit
    public Skit() { }
    public Skit(YarnProgram fileKey, string scene, bool play, string interactWith = "")
    {
        skitFileKey = fileKey;
        this.scene = scene;
        playOnSceneLoad = play;
        interactLoad = interactWith;
    }

    public YarnProgram GetSkitFile()
    {
        return skitFileKey;
    }
}