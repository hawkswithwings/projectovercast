﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save
{
    

    public float playerX = 0.0f;
    public float playerY = 0.0f;

    public List<float> npcX = new List<float>();
    public List<float> npcY = new List<float>();
    public List<int> travelPoint = new List<int>();
    public List<int> npcDirection = new List<int>();


    //Whatever we need to save should be in here.
}
