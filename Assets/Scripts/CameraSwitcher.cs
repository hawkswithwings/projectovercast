﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraSwitcher : MonoBehaviour
{
    public CinemachineVirtualCamera mainCamera;
    private PlayerMovement playerMovement;
    private InventoryInteraction plotInspection;
    private int priority;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        if(mainCamera == null)
        {
            mainCamera = GameObject.FindGameObjectWithTag("PlayerVCam").GetComponent<CinemachineVirtualCamera>();
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            playerMovement = player.GetComponent<PlayerMovement>();
            plotInspection = player.GetComponent<InventoryInteraction>();
            playerMovement.toggleStayOff();
            priority = mainCamera.Priority;
        }
        SwitchCameras();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SwitchCameras()
    {
        if (mainCamera.Priority > 0)
            mainCamera.Priority = 0;
        else
            mainCamera.Priority = priority;

        plotInspection.enabled = !plotInspection.enabled;
        if (mainCamera.Priority > 0)
        {
            playerMovement.toggleStayOff();
            playerMovement.ActivateMovement();
        }
    }
}
