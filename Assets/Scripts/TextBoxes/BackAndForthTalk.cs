﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackAndForthTalk : TalkativeNPC
{
    public bool removeWhenDone;

    private struct dialogueInfo {
        public string name;
        public string dialogue;
    };
    private bool childTalking;
    private bool childMoreToSay;

    private string currentStatement;
    private string currentName;
    public int index;
    private List<dialogueInfo> dialogues;

    private string filePath = "Assets/Dialogue/";

    // Start is called before the first frame update
    new public void Start()
    {
        textBox = GameObject.FindGameObjectWithTag("TextBox").GetComponent<DialogueBox>();
        childTalking = true;
        childMoreToSay = true;
        string[] textLines = System.IO.File.ReadAllLines(textPath);
        dialogues = new List<dialogueInfo>();
        for(int i = 0; i < textLines.Length; i++)
        {
            if (textLines[i].Equals("**NAME**"))
            {
                dialogueInfo di = new dialogueInfo
                {
                    name = textLines[i + 1],
                    dialogue = textLines[i+3]
                };
                dialogues.Add(di);
                i += 3; 
            }
        }
        index = 0;

        currentStatement = dialogues[index].dialogue;
        currentName = dialogues[index].name;
        //SetNameAndStatement(currentName, currentStatement);
        InitMovement();
    }

    override public bool NextLine()
    {
        if (textBox.transform.localScale.x < 1.0f)
        {
            moving = false;
            StartCoroutine(DisplayStatement());
            childTalking = true;
        }
        else if (childTalking)
        {
            childTalking = textBox.DisplayNextLine();
            //true if there's more text to display, false otherwise

        }
        if (!childTalking && index < dialogues.Count-1)
        {
            index++;
            currentStatement = dialogues[index].dialogue;
            currentName = dialogues[index].name;
            textBox.SetTextDisplay(currentStatement);
            textBox.SetName(currentName);
            textBox.StartTalking();
            childTalking = true;
        }
        else if (textBox.transform.localScale.x >= 1.0f && !childTalking && index >= dialogues.Count-1)
        {
            StartCoroutine(HideStatement());
            if(talkIcon !=null)
                talkIcon.SetActive(true);
            //SetNameAndStatement(currentName, currentStatement);
            //if (!touchingPlayer)
            //moving = true;
        }

        return !childTalking;
        //Return determines if player can move. If childTalking, player's canMove should be false, if done, canMove should be true.

    }

    public void UpdateFile(string fileToRead)
    {
        textPath = filePath + fileToRead + ".txt";
    }

    public IEnumerator removeThis()
    {
        yield return new WaitUntil(() => textBox.transform.localScale.x <= 0.0f);
        Destroy(GetComponent<CircleCollider2D>());
        Destroy(this);
    }
}
