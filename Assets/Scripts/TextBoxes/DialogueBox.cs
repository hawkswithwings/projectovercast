﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class DialogueBox : TextBoxActions
{
    public Text dialogueName;

    private string npcDialogue;
    private int characterLimit = 80;
    private int dialogueIndex;
    private bool talking;
    private float talkTime;
    public float maxTalkTime;

    [SerializeField] private Text displayedText;
    [SerializeField] private Text displayedName;

    // Start is called before the first frame update
    public override void Start()
    {
        talking = false;
        dialogueIndex = 0;
        talkTime = maxTalkTime;
        base.Start();
        
    }

    // Update is called once per frame
    public override void Update()
    {
        if(!talking)
        {
            base.Update();
            
        }
        else if(talking){
            talkTime -= Time.deltaTime;
            if (talkTime <= 0.0f)
            {
                displayedText.text = npcDialogue.Substring(0, dialogueIndex);
                dialogueIndex++;
                bool stop = false;
                if (dialogueIndex < npcDialogue.Length)
                {
                    if (npcDialogue[dialogueIndex-1] == ' ' && (characterLimit - dialogueIndex > 0 && dialogueIndex > characterLimit-5))
                    {
                        stop = true;
                    }
                }
                if (dialogueIndex > npcDialogue.Length || dialogueIndex > characterLimit || stop)
                {
                    /*if (dialogueIndex > characterLimit)
                        if (npcDialogue[dialogueIndex - 1] != ' ')
                            displayedText.text += "-";*/

                    talking = false;
                    if(!stop)
                        dialogueIndex--;
                }
                talkTime = maxTalkTime;
            }
        }
    }

    public void SetTextDisplay(string dialogue)
    {
        npcDialogue = dialogue;
    }

    public void StartTalking()
    {
        talking = true;
        dialogueIndex = 0;
    }

    public override void HideTextBox()
    {
        base.HideTextBox();
    }

    public bool DisplayNextLine()
    {
        if (talking)
        {
            //Fill statement
            dialogueIndex = characterLimit;
            if (dialogueIndex > npcDialogue.Length)
            {
                dialogueIndex = npcDialogue.Length;
            }
            displayedText.text = npcDialogue.Substring(0, dialogueIndex);
            if(dialogueIndex == characterLimit)
            {
                int spaceSpot = displayedText.text.IndexOf(" ", characterLimit - 5);
                if (spaceSpot < characterLimit - 1 && spaceSpot != -1)
                {
                    displayedText.text = displayedText.text.Remove(spaceSpot);
                    dialogueIndex = spaceSpot + 1;
                }
            }
            
            /*if (npcDialogue[dialogueIndex - 1] != ' ' && dialogueIndex != npcDialogue.Length)
            {
                displayedText.text += "-";
            }*/
            talking = false;
            talkTime = 0.05f;

            return true;
        }
        else if (dialogueIndex < npcDialogue.Length) //there's still dialogue to display
        {
            npcDialogue = npcDialogue.Substring(dialogueIndex);
            dialogueIndex = 1;
            talking = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetName(string name)
    {
        dialogueName.text = name;
        dialogueName.transform.parent.gameObject.SetActive(true);
    }

    public void RemoveName()
    {
        dialogueName.text = "";
        dialogueName.transform.parent.gameObject.SetActive(false);
    }

    public void SetNameYarn(string name)
    {
        dialogueName.text = name;
        dialogueName.transform.parent.gameObject.SetActive(true);
        //runner.dialogueUI.
    }
}
