﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBoxActions : MonoBehaviour
{
    private bool grow;
    private bool shrink;
    // Start is called before the first frame update

    /*private string npcDialogue;
    private int characterLimit = 80;
    private int dialogueIndex;
    private bool talking;
    private float talkTime;
    public float maxTalkTime;

    [SerializeField]private Text displayedText;*/
    public virtual void Start()
    {
        //grow = false;
        //shrink = false;
        /*talking = false;
        dialogueIndex = 0;
        talkTime = maxTalkTime;*/
    }

    // Update is called once per frame
    public virtual void Update()
    {
        if (grow)
        {
            Vector3 newScale = new Vector3(0.1f, 0.1f, 0.1f);
            transform.localScale += newScale;
            if(transform.localScale.x >= 1.0f)
            {
                transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                grow = false;
                //talking = true;
                //dialogueIndex = 0;
            }
        }

        /*else if (talking)
        {
            talkTime -= Time.deltaTime;
            if(talkTime <= 0.0f)
            {
                displayedText.text = npcDialogue.Substring(0, dialogueIndex);
                dialogueIndex++;
                if (dialogueIndex > npcDialogue.Length || dialogueIndex > characterLimit)
                {
                    if(dialogueIndex > characterLimit)
                        if (npcDialogue[dialogueIndex - 1] != ' ')
                            displayedText.text += "-";

                    talking = false;
                    dialogueIndex--;
                }
                talkTime = maxTalkTime;
            }
            
        }*/

        else if (shrink)
        {
            Vector3 newScale = new Vector3(0.1f, 0.1f, 0.1f);
            transform.localScale -= newScale;
            if (transform.localScale.x <= 0.0f)
            {
                transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
                shrink = false;
            }
        }
    }

    public virtual void ShowTextBox()
    {
        grow = true;
    }

    public virtual void HideTextBox()
    {
        //displayedText.text = "";
        shrink = true;
    }

    /*public void SetTextDisplay(string dialogue)
    {
        npcDialogue = dialogue;
    }*/

    public bool IsGrowingOrShrinking()
    {
        return (grow || shrink);
    }

    /*public bool DisplayNextLine()
    {
        if (talking)
        {
            //Fill statement
            dialogueIndex = characterLimit;
            if(dialogueIndex > npcDialogue.Length)
            {
                dialogueIndex = npcDialogue.Length;
            }
            displayedText.text = npcDialogue.Substring(0, dialogueIndex);
            if (npcDialogue[dialogueIndex-1] != ' ' && dialogueIndex != npcDialogue.Length)
            {
                displayedText.text += "-";
            }
            talking = false;
            talkTime = 0.05f;
            
            return true;
        }
        else if(dialogueIndex < npcDialogue.Length) //there's still dialogue to display
        {
            npcDialogue = npcDialogue.Substring(dialogueIndex);
            dialogueIndex = 1;
            talking = true;
            return true;
        }
        else
        {
            return false;
        }
    }*/
}
