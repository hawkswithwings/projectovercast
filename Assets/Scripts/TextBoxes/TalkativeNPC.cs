﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

[SerializeField]
public class TalkativeNPC : MonoBehaviour
{
    public DialogueRunner runner;
    public YarnProgram scriptToLoad;
    private ModifiedDialogueUI dialogueUI;
    public bool waitToLoad;

    //public TextAsset statementFile;
    public string npcName;
    public string textPath;

    public Animator animations;


    public bool moving;
    public Vector2[] travelPoints;
    private Vector2 direction;
    private int travelIndex;
    private int nextTravelPoint;
    private float speed;
    private float moveWait;

    public bool touchingPlayer;

    public DialogueBox textBox;
    public GameObject talkIcon;

    private float talkTimer;

    private bool talking;

    private bool moreToSay;

    private bool talked;

    // Start is called before the first frame update
    public void Start()
    {
        textBox = GameObject.FindGameObjectWithTag("TextBox").GetComponent<DialogueBox>();
        talkTimer = 0.3f;

        travelIndex = 0;
        if (travelPoints.Length > 0)
            direction = travelPoints[travelIndex] - (Vector2)transform.position;
        else
            direction = new Vector2();
        direction.Normalize();
        UpdateAnim();
        speed = 3.0f;
        nextTravelPoint = 1;
        moveWait = 0.0f;

        talking = false;
        moreToSay = true;

        runner = FindObjectOfType<DialogueRunner>();
        dialogueUI = runner.gameObject.GetComponent<ModifiedDialogueUI>();
        if (scriptToLoad != null && !waitToLoad)
            runner.Add(scriptToLoad);

        talked = false;

        //statement = statementFile.text;
    }

    private void OnDisable()
    {
        if(talkIcon != null)
            talkIcon.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void loadScript()
    {
        runner.Add(scriptToLoad);
    }

    public void InitMovement()
    {
        travelIndex = 0;
        speed = 3.0f;
        nextTravelPoint = 1;
        moveWait = 0.0f;
        if (travelPoints.Length > 0)
            direction = travelPoints[travelIndex] - (Vector2)transform.position;
        else
            direction = Vector2.right;
        direction.Normalize();
        UpdateAnim();
    }

    // Update is called once per frame
    void Update()
    {
		if(talkIcon != null && !talking)
        {
			if(talkIcon.activeSelf == true)
			{
				talkTimer -= Time.deltaTime;
				if(talkTimer <= 0.0f)
				{
					talkTimer = 0.5f;
					talkIcon.GetComponent<SpriteRenderer>().enabled = !talkIcon.GetComponent<SpriteRenderer>().enabled;
				}
			}
		}

        if(moveWait > 0.0f && moving && travelPoints.Length > 0)
        {
            moveWait -= Time.deltaTime;

            if(moveWait <= 0.0f)
            {
                direction = travelPoints[travelIndex] - (Vector2)transform.position;
                direction.Normalize();
                UpdateAnim();
            }
        }

    }

    private void FixedUpdate()
    {
        if (moving && moveWait <= 0.0f)
        {
            transform.position = Vector2.MoveTowards((Vector2)transform.position, travelPoints[travelIndex], speed * Time.fixedDeltaTime);

            if ((Vector2)transform.position == travelPoints[travelIndex])
            {
                travelIndex += nextTravelPoint;
                if(travelIndex+nextTravelPoint == travelPoints.Length || travelIndex+nextTravelPoint == -1)
                {
                    nextTravelPoint *= -1;
                }
                
                moveWait = 1.0f;
                UpdateAnim();
            }
        }
    }

    private void UpdateAnim()
    {

        if (moveWait < 0.01f && animations != null)
        {
            animations.SetFloat("XSpeed", direction.x);
            animations.SetFloat("YSpeed", direction.y);
            animations.SetFloat("Magnitude", direction.magnitude);
        }
        else if(moveWait > 0 && animations != null)
        {
            if (moveWait < 0.01f)
            {
                animations.SetFloat("XSpeed", direction.x);
                animations.SetFloat("YSpeed", direction.y);
                animations.SetFloat("Magnitude", direction.magnitude);
            }
            else if (moveWait > 0)
            {
                animations.SetFloat("IdleX", direction.x);
                animations.SetFloat("IdleY", direction.y);
                animations.SetFloat("Magnitude", 0);
            }
        }
    }

    public IEnumerator DisplayStatement()
    {
        textBox.SetName(npcName);
        textBox.ShowTextBox();
        yield return new WaitUntil(() => textBox.transform.localScale.x >= 1.0f);
        textBox.StartTalking();
    }

    public IEnumerator HideStatement()
    {
        textBox.HideTextBox();
        yield return new WaitUntil(() => textBox.transform.localScale.x <= 0.0f);
        textBox.RemoveName();
    }

    public void NewHideStatement()
    {
        textBox.RemoveName();
        dialogueUI.onDialogueEnd.RemoveListener(NewHideStatement);
        talking = false;
    }

    virtual public bool OldNextLine()
    {
        if (textBox.transform.localScale.x < 1.0f)
        {
            moving = false;
            moreToSay = true;
            talkIcon.SetActive(false);
            StartCoroutine(DisplayStatement());
            talking = true;

        }
        else if (talking)
        {
            talking = textBox.DisplayNextLine();
            //true if there's more text to display, false otherwise

        }
        if (textBox.transform.localScale.x >= 1.0f && !talking && !moreToSay)
        {
            StartCoroutine(HideStatement());
            talkIcon.SetActive(true);
            if(!touchingPlayer && travelPoints.Length > 0)
                moving = true;
        }

        return !talking;
        //Return determines if player can move. If talking, player's canMove should be false, if done, canMove should be true.

    }

    virtual public bool NextLine()
    {
        if (!talking)
        {
            dialogueUI.onDialogueEnd.AddListener(NewHideStatement);
            talking = true;
            runner.variableStorage.SetValue("$talked", new Yarn.Value(talked));
            talked = true;
            runner.StartDialogue(scriptToLoad.name);
            talkIcon.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            dialogueUI.MarkLineComplete();
        }

        return !talking;
        //talking = true means canMove = false
    }

    public void AboutThis(int choice)
    {
        /*switch (choice)
        {
            case 0:
                AboutThem();
                break;
            case 1:
                AboutItem();
                break;
            case 2:
                AboutPlace();
                break;
            case 3:
                SayGoodBye();
                break;
        }

        textBox.SetTextDisplay(statement);
        textBox.StartTalking();
        talking = true;*/
        dialogueUI.SelectOption(choice);
    }

    public bool HasMoreToSay()
    {
        return moreToSay;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            moving = false;
            touchingPlayer = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (travelPoints.Length > 1)
            {
                moving = true;
                moveWait = 0.5f;
            }
            touchingPlayer = false;
            
        }
    }

    public void SetTravelIndex(int ti)
    {
        travelIndex = ti;
        moving = true;
        moveWait = 0.0f;
        direction = travelPoints[travelIndex] - (Vector2)transform.position;
        direction.Normalize();
        UpdateAnim();
    }

    public int GetTravelIndex()
    {
        return travelIndex;
    }

    public int GetDirection()
    {
        return nextTravelPoint;
    }

    public void SetDirection(int d)
    {
        nextTravelPoint = d;
    }

    public DialogueBox GetTextBox()
    {
        return textBox;
    }

    public void UpdateFile(YarnProgram newScript)
    {
        scriptToLoad = newScript;
        if(runner != null)
            runner.Add(newScript);
    }

    public IEnumerator removeThis()
    {
        yield return new WaitUntil(() => textBox.transform.localScale.x <= 0.0f);
        Destroy(GetComponent<CircleCollider2D>());
        Destroy(this);
    }
}
