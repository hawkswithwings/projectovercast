﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueOptions : TextBoxActions
{
    public GameObject cursor;
    //actual cursor object to reposition with input
    public Vector2[] cursorSpots;
    //three spots the cursor can be, 0 is top, 1 is middle, 2 is bottom

    private int cursorIndex;

    private float inputWait;


    public override void Start()
    {

        cursorIndex = 0;

        float startY = cursor.transform.localPosition.y;
        for(int i = 0; i < cursorSpots.Length; i++)
        {
            cursorSpots[i].x = cursor.transform.localPosition.x;
            cursorSpots[i].y = startY;
            startY -= 30.0f;
        }

        inputWait = 0.0f;

        base.Start();
    }

    public override void Update()
    {
        if (IsGrowingOrShrinking())
        {
            base.Update();
        }
        else if(transform.localScale.x >= 1.0f)
        {
            inputWait -= Time.deltaTime;
            if(inputWait <= 0.0f && Input.GetAxisRaw("Vertical") > 0.0f)
            {
                cursorIndex--;
                inputWait = 0.2f;
                if (cursorIndex <= -1)
                    cursorIndex = cursorSpots.Length - 1;
            }
            else if (inputWait <= 0.0f && Input.GetAxisRaw("Vertical") < 0.0f)
            {
                cursorIndex++;
                inputWait = 0.2f;
                
                if (cursorIndex >= cursorSpots.Length)
                    cursorIndex = 0;
            }

            if((Vector2)cursor.transform.localPosition != cursorSpots[cursorIndex])
            {
                cursor.transform.localPosition = cursorSpots[cursorIndex];
            }
        }
    }

    public int GetSelectedOption()
    {
        /*
         * 0 means ask about NPC
         * 1 means ask about item
         * 2 means ask about place
         * 3 means good bye
         *
         * If it's yes or no, 0 is yes, 1 is no.
         */
        return cursorIndex;
    }

    public void ResetCursor()
    {
        if (cursorIndex == cursorSpots.Length - 1)
        {
            cursorIndex = 0;
            cursor.transform.localPosition = cursorSpots[0];
        }
    }

    public override void ShowTextBox()
    {
        ResetCursor();
        base.ShowTextBox();
    }

}
