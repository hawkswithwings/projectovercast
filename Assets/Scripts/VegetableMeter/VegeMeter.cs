﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegeMeter : MonoBehaviour
{
    public GameObject frontBar;
    public GameObject middleBar;
    public float firstSpeed;
    public float secondSpeed;

    public int numPoints;

    private int currentPoints;
    private float pointLength;

    private Vector2 target;
    private bool middleMoving, moving, useSecondSpeed;

    private Dictionary<string, int> devoured;

    // Start is called before the first frame update
    void Start()
    {
        devoured = new Dictionary<string, int>();
        currentPoints = 0;
        pointLength = frontBar.GetComponent<RectTransform>().rect.width;
        pointLength /= numPoints;
        moving = false;
        middleMoving = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            GameObject barToMove = frontBar;
            if (middleMoving)
                barToMove = middleBar;
            float speed = firstSpeed;
            if (useSecondSpeed)
                speed = secondSpeed;

            barToMove.transform.localPosition = Vector3.MoveTowards(barToMove.transform.localPosition, target, speed * Time.deltaTime);
            if (barToMove.transform.localPosition.x == target.x)
            {
                if (middleMoving) //middle has finished, start front
                {
                    middleMoving = false;
                    useSecondSpeed = true;
                }
                else if (middleBar.transform.localPosition.x != target.x) //middle hasn't moved, so front has finished, move middle
                {
                    middleMoving = true;
                    useSecondSpeed = false;
                }
                else //both have finished moving
                {
                    moving = false;
                    middleMoving = false;
                    useSecondSpeed = false;
                }
            }
        }
    }

    public void AdjustMeter(int amount)
    {
        if (!moving)
        {
            if (currentPoints + amount > numPoints)
                amount = numPoints - currentPoints;
            else if (currentPoints + amount < 0)
                amount = 0 - currentPoints;

            if (amount == 0)
                return;

            currentPoints += amount;

            if (amount > 0) //move middle first
            {
                middleMoving = true;
                useSecondSpeed = false;
            }
            else //decreasing, so front should go back followed by the middle
            {
                middleMoving = false;
                useSecondSpeed = true;
            }

            target = frontBar.transform.localPosition;
            float changeX = amount * pointLength;
            target.x += changeX;
            Debug.Log(target.x);
            moving = true;
            
        }
    }

    public void EatMeal(List<string> veggies)
    {
        moving = true;
        //Allocate how many spaces veggies get each
        int allocate = numPoints / veggies.Count;
        // With two veggies, we get 5 / 2 = 2.5 ~ 2
        // Our index starts at 0
        int curVegIndex = 0;
        // Our positions start at 0
        int counter = 0;
        for (int x = 0; x < numPoints; x++)
        {
            string curVeg = veggies[curVegIndex];
            if (devoured.ContainsKey(curVeg))
            {
                devoured[curVeg]++;
            }
            else
            {
                devoured[curVeg] = 1;
            }
            
            counter++;
            //Every time counter hits the allocation number, we reset it and increase our curVegIndex
            if(counter == allocate)
            {
                counter = 0;
                curVegIndex++;
                if (curVegIndex >= veggies.Count)
                    curVegIndex = veggies.Count - 1;
                // If there's an odd number, just give the overflow to the last one
            }
        }
        AdjustMeter(numPoints);
        //Need to add graphics per veggie.
    }

    public bool IsStillMoving()
    {
        return moving;
    }
}
