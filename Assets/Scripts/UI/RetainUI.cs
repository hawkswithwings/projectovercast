﻿using UnityEngine;

public class RetainUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        if(GameObject.FindGameObjectsWithTag("UI").Length > 2)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoadManager.DontDestroyOnLoad(this.gameObject);
    }
}
