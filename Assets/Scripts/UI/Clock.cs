﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    public bool advanceClock;
    private const float REAL_SECONDS_PER_INGAME_DAY = 5.0f;
    private Transform clockHand;
    public Text TimeText;
    private bool IsMorning;
    private int hours;
    private float day;

    private int curHour;
    private bool curMorn;

    public Light Sun;
    public Color[] TimeOfDay;


    private void Awake()
    {
        clockHand = transform.Find("ClockHand");
        IsMorning = true;
        hours = 0;
        curHour = 0;
        curMorn = true;
        Sun.color = TimeOfDay[0];

        if (!advanceClock)
        {
            Sun.color = TimeOfDay[1];
            hours = 0;
            TimeText.text = "00 PM";
        }
            
    }

    
    public int getHour()
    {
        return hours;
    }

    public void SetHour(int hour, bool isAm)
    {
        if(hour == 12)
            clockHand.eulerAngles = new Vector3(0, 0, -30.0f * hour);
        
        else
            clockHand.eulerAngles = new Vector3(0, 0, -30.0f * hour);

        hours = hour;
        IsMorning = isAm;

        string timeOfDay = "Witching Hour";

        if (isAm)
        {
            if(hour < 5 && hour > 0)
            {
                timeOfDay = "Night";
            }
            else if (hour < 8 && hour >= 5)
            {
                timeOfDay = "Dawn";
            }

            else if (hour < 12 && hour >= 8)
            {
                timeOfDay = "Morning";
            }
            else if (hour == 12 || hour == 0)
            {
                timeOfDay = "Witching Hour";
            }
        }
        else
        {
            if (hour <= 5 && hour > 0)
            {
                timeOfDay = "Afternoon";
            }
            else if (hour < 9 && hour > 5)
            {
                timeOfDay = "Evening";
            }

            else if (hour < 12 && hour >= 9)
            {
                timeOfDay = "Night";
            }
            else if (hour == 0 || hour == 12)
            {
                timeOfDay = "High Noon";
            }
        }
        UpdateWorld(timeOfDay);
    }

    public void UpdateWorld(string x)
    {
        /*string period = "AM";
        if (!IsMorning)
            period = "PM";*/
       //TimeText.text = x + " " + period;

        TimeText.text = x;

        if (hours == 10 && IsMorning)
        {
            Sun.color = TimeOfDay[1];
        }
        else if(hours == 6 && !IsMorning)
        {
            Sun.color = TimeOfDay[2];
        }
        else if (hours == 5 && IsMorning)
        {
            Sun.color = TimeOfDay[0];
        }
    }
}
