﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObjects : MonoBehaviour
{
    public List<TalkativeNPC> objectsToActivate;

    void OnEnable()
    {
        for (int i = 0; i < objectsToActivate.Count; i++)
        {
            objectsToActivate[i].enabled = !objectsToActivate[i].enabled;

            if (objectsToActivate[i].enabled)
            {
                objectsToActivate[i].loadScript();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
