﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CinematicSceneLoader : MonoBehaviour
{
    public string sceneToLoad;
    // Start is called before the first frame update
    private void OnEnable()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
