﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Cinemachine;

public class TimelineFiller : MonoBehaviour
{
    public YarnProgram yarnProgram;
    public CinemachineVirtualCamera followCam;
    public CutsceneConvoStart cutsceneConvo;
    private PlayableDirector director;

    // Start is called before the first frame update
    void Start()
    {
        director = GetComponent<PlayableDirector>();
        Vector3 position = GameObject.FindGameObjectWithTag("PlayerCutscenePos").transform.position;

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.transform.position = position;
        player.GetComponent<PlayerMovement>().enabled = false;

        if(yarnProgram != null)
        {
            TalkingToYourselfTemp playerTalk = player.GetComponent<TalkingToYourselfTemp>();
            playerTalk.UpdateYarnFile(yarnProgram);
            cutsceneConvo.convo = playerTalk;
        }

        Animator playerAnimator = player.GetComponent<Animator>();
        if(followCam != null)
            followCam.Follow = player.transform;

        GameObject blackFade = GameObject.FindGameObjectWithTag("FadeToBlack");
        Animator fadeAnimator = blackFade.GetComponent<Animator>();

        TimelineAsset asset = director.playableAsset as TimelineAsset;
        var tracks = asset.GetOutputTracks();
        foreach(var track in tracks)
        {
            if (track.ToString().Contains("Player"))
                director.SetGenericBinding(track, playerAnimator);
            if (track.ToString().Contains("Fade"))
                director.SetGenericBinding(track, fadeAnimator);
        }

        director.Play(director.playableAsset);
    }
}
