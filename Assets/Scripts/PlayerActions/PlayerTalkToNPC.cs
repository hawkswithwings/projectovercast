﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTalkToNPC : MonoBehaviour
{
    public DialogueOptions dOptions;
    private TalkativeNPC currentNPC;
    private PlayerMovement playerMovement;
    private InventoryInteraction inventoryInteraction;

    private bool canMove;
    private bool choosing;

    public Inventory backpack;

    // Start is called before the first frame update
    void Start()
    {
        playerMovement = GetComponent<PlayerMovement>();
        inventoryInteraction = GetComponent<InventoryInteraction>();
        choosing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && canMove)
        {
            playerMovement.SetTalkDir();
            playerMovement.enabled = false;
            canMove = false;
        }

        if (Input.GetButtonDown("Fire1") && !choosing && canMove == false)
        {
            //Debug.Break();
            canMove = currentNPC.NextLine();

            /*if (canMove && currentNPC.HasMoreToSay())
            {
                canMove = false;
                choosing = true;
                StartCoroutine(DisplayDialogueOptions());
            }*/
        }

        else if (Input.GetButtonDown("Fire1") && choosing)
        {
            //StartCoroutine(HideDialogueOptions());
            choosing = false;

            int choice = dOptions.GetSelectedOption();
            //0 means ask about NPC
            //1 means ask about item
            //2 means ask about place
            //3 means good bye
            dOptions.ResetCursor();

            currentNPC.AboutThis(choice);
        }

        if (canMove == true)
        {
            Debug.Log("PlayerTalkToNPC's canMove is true");
            playerMovement.ActivateMovement();
            if (currentNPC != null)
            {
                if (currentNPC.gameObject.CompareTag("FarmPlot"))
                {
                    //remove BackAndForthTalk component and trigger, deactivate PlayerTalkToNPC
                    StartCoroutine(currentNPC.gameObject.GetComponent<TalkativeNPC>().removeThis());
                    playerMovement.EndBaFInteraction();
                }
            }
        }
    }

    public void cutTalkShort()
    {
        currentNPC = null;
        this.enabled = false;
    }

    private void OnEnable()
    {
        if(dOptions == null)
            dOptions = GameObject.FindGameObjectWithTag("DialogueOptions").GetComponent<DialogueOptions>();
    }

    private IEnumerator DisplayDialogueOptions()
    {
        dOptions.ShowTextBox();
        yield return new WaitUntil(() => dOptions.transform.localScale.x >= 1.0f);
    }

    private IEnumerator HideDialogueOptions()
    {
        dOptions.HideTextBox();
        yield return new WaitUntil(() => dOptions.transform.localScale.x <= 0.0f);
    }
    private IEnumerator ShowBackpack()
    {
        backpack.ShowTextBox();
        yield return new WaitUntil(() => backpack.transform.localScale.x >= 1.0f);
    }

    public void SetCurrentNPC(TalkativeNPC npc)
    {
        currentNPC = npc;
        canMove = false;
    }

    public void StartChoosing()
    {
        choosing = true;
    }

    public void CanNowMove()
    {
        if (this.enabled)
            canMove = true;
    }

    public void FinishUsingFridge(bool allowMovement)
    {
        choosing = false;
        if (allowMovement)
        {
            Debug.Log("PlayerTalkToNPC's FinishUsingFridge activating movement");
            canMove = true;
            playerMovement.ActivateMovement();
        }
        this.enabled = true;
    }
}
