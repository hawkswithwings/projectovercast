﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //public DialogueOptions dOptions;
    public float speed;
    public Animator animations;

    private Vector2 movement;

    private Rigidbody2D rb;

    private TalkativeNPC currentNPC;

    private bool canMove;
    private bool choosing;

    public Inventory backpack;

    private Vector2 idleDir;

    private PlayerTalkToNPC playerTalkToNPC;

    private InventoryInteraction invInter;

    private bool stayOff;

    // Start is called before the first frame update
    void Start()
    {
        canMove = true;
        rb = GetComponent<Rigidbody2D>();
        choosing = false;

        idleDir = Vector2.zero;

        playerTalkToNPC = GetComponent<PlayerTalkToNPC>();
        invInter = GetComponent<InventoryInteraction>();
        stayOff = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && currentNPC != null && !choosing)
        {
            SetTalkDir();

            rb.velocity = Vector2.zero;
            this.enabled = false;
        }

        else if (Input.GetButtonDown("Fire2"))
        {
            rb.velocity = Vector2.zero;
            StartCoroutine(ShowBackpack());
            this.enabled = false;
        }

        if (canMove)
        {
            //movement = new Vector2();
            float x = Input.GetAxisRaw("Horizontal");
            float y = Input.GetAxisRaw("Vertical");

            if(x != 0 || y != 0)
                SetDir(movement.x, movement.y);

            movement.x = x;
            movement.y = y;

            animations.SetFloat("YSpeed", movement.y);
            animations.SetFloat("XSpeed", movement.x);
            animations.SetFloat("Magnitude", movement.magnitude);

            //movement *= speed * Time.deltaTime;   

            
        }
        else
        {
            movement = new Vector2();

            animations.SetFloat("YSpeed", movement.y);
            animations.SetFloat("XSpeed", movement.x);
            animations.SetFloat("Magnitude", movement.magnitude);
        }

     
    }

    private void OnEnable()
    {
        //if (stayOff)
            //this.enabled = false;
    }

    private void OnDisable()
    {
        animations.SetFloat("YSpeed", 0);
        animations.SetFloat("XSpeed", 0);
        animations.SetFloat("Magnitude", 0);
    }

    public void SetTalkDir()
    {
        if (currentNPC != null)
        {
            Vector2 dir = (currentNPC.transform.position - transform.position).normalized;

            animations.SetFloat("XSpeed", 0);
            animations.SetFloat("YSpeed", 0);
            animations.SetFloat("IdleX", dir.x);
            animations.SetFloat("IdleY", dir.y);
        }
    }

    private void SetDir(float x, float y)
    {
        idleDir = new Vector2(x, y);

        animations.SetFloat("IdleX", idleDir.x);
        animations.SetFloat("IdleY", idleDir.y);
    }

    void FixedUpdate()
    {
        if (canMove)
        {
            rb.velocity = movement * speed * Time.deltaTime;
            //rb.MovePosition((Vector2)transform.position + movement);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("NPC") || (collision.gameObject.CompareTag("FarmPlot") || collision.gameObject.CompareTag("InvInteract") || collision.gameObject.CompareTag("Interactable") || collision.gameObject.CompareTag("FarmPlot") && collision.gameObject.GetComponent<TalkativeNPC>() != null))
        {
            currentNPC = collision.gameObject.GetComponent<TalkativeNPC>();
            
            if(currentNPC.talkIcon != null)
                currentNPC.talkIcon.SetActive(true);
            
            if(playerTalkToNPC != null)
            {
                playerTalkToNPC.enabled = true;
                playerTalkToNPC.SetCurrentNPC(currentNPC);
            }
            
            if (collision.gameObject.CompareTag("InvInteract"))
            {
                invInter.enabled = true;
                invInter.usingFridge = true;
            }
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("NPC") || (collision.gameObject.CompareTag("FarmPlot") || collision.gameObject.CompareTag("InvInteract") || collision.gameObject.CompareTag("Interactable") || collision.gameObject.CompareTag("FarmPlot") && collision.gameObject.GetComponent<TalkativeNPC>() != null))
        {
            if(currentNPC != null)
            {
                if (currentNPC.talkIcon != null)
                    currentNPC.talkIcon.SetActive(false);
                
                currentNPC = null;
                if(playerTalkToNPC != null)
                {
                    playerTalkToNPC.enabled = false;
                    playerTalkToNPC.SetCurrentNPC(null);
                }
            }
            if (collision.gameObject.CompareTag("InvInteract"))
            {
                invInter.enabled = false;
                invInter.usingFridge = false;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("FarmPlot"))
        {
            //if (collision.gameObject.GetComponent<TalkativeNPC>() == null)
            invInter.enabled = true;
            invInter.plot = collision.gameObject.GetComponent<Plot>();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("FarmPlot"))
        {
            invInter.enabled = true;
            invInter.plot = null;
        }
    }

    private IEnumerator ShowBackpack()
    {
        backpack.ShowTextBox();
        yield return new WaitUntil(() => backpack.transform.localScale.x >= 1.0f);
    }

    public void EndBaFInteraction()
    {
        currentNPC = null;
        playerTalkToNPC.enabled = false;
        playerTalkToNPC.SetCurrentNPC(null);
    }

    public void toggleStayOff()
    {
        stayOff = !stayOff;
    }

    public void ActivateMovement()
    {
        if(stayOff == false)
            this.enabled = true;
    }
}
