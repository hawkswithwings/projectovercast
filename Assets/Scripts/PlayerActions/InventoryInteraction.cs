﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class InventoryInteraction : MonoBehaviour
{
    private PlayerMovement playerMovement;
    private PlayerTalkToNPC talkToNpc;
    private ModifiedDialogueUI dialogueUI;

    public Plot plot = null;
    public bool usingFridge = false;
    public Inventory backpack;
    public bool wait;
    public DialogueOptions yesOrNo;

    private bool choosing;
    private Animator animations;
    private bool canMove;

    // Start is called before the first frame update
    void Start()
    {
        talkToNpc = GetComponent<PlayerTalkToNPC>();
        playerMovement = GetComponent<PlayerMovement>();
        usingFridge = false;
        wait = false;
        choosing = false;
        animations = GetComponent<Animator>();

        dialogueUI = FindObjectOfType<DialogueRunner>().gameObject.GetComponent<ModifiedDialogueUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if(plot != null)
        {
            //Needs to check for fire1 while plot isn't null
            if(plot.GetComponent<TalkativeNPC>() != null)
            {
                return;
            }
            if (backpack.transform.localScale.x <= 0.0f)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    if (!choosing)
                    {
                        playerMovement.enabled = false;
                        animations.SetFloat("YSpeed", 0.0f);
                        animations.SetFloat("XSpeed", 0.0f);
                        plot.DisplayMessage();
                    }
                    else if (choosing)
                    {
                        choosing = false;
                        int choice = yesOrNo.GetSelectedOption();
                        plot.ChooseOption(choice);
                    }
                }
            }

            //This happens when the backpack is opened by Yarn
            else if (backpack.transform.localScale.x >= 1.0f)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    if (choosing)
                    {
                        choosing = false;
                        int choice = yesOrNo.GetSelectedOption();
                        plot.ChooseOption(choice);
                    }
                }
            }
        }

        else if (usingFridge)
        {
            if (backpack.transform.localScale.x <= 0.0f)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    if (!choosing)
                    {
                        playerMovement.enabled = false;
                        animations.SetFloat("YSpeed", 0.0f);
                        animations.SetFloat("XSpeed", 0.0f);
                        //hide backback and display message
                    }
                    else if (choosing)
                    {
                        choosing = false;
                        int choice = yesOrNo.GetSelectedOption();
                        dialogueUI.SelectOption(choice);
                    }
                }
            }

            //This happens when the backpack is opened by Yarn
            else if (backpack.transform.localScale.x >= 1.0f)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    if (choosing)
                    {
                        choosing = false;
                        int choice = yesOrNo.GetSelectedOption();
                        dialogueUI.SelectOption(choice);
                    }
                }
            }
        }

        /*if(Input.GetButtonDown("Fire1") && plot != null)
        {   
            if (!wait)
            {
                playerMovement.enabled = false;
                animations.SetFloat("YSpeed", 0.0f);
                animations.SetFloat("XSpeed", 0.0f);
                plot.DisplayMessage();
                //wait = true;
            }
            else if(!choosing)
            {
                choosing = plot.DisplayOptions();
                if(choosing && !plot.BoxesDisplayed())
                {
                    choosing = false;
                    wait = false;
                    playerMovement.enabled = true;
                    GetComponent<PlotInfo>().UpdatePlotInfo();
                }
            }
            else if(choosing && plot.BoxesDisplayed())
            {
                choosing = plot.PerformSelectedAction();
                if (choosing == false)
                {
                    wait = false;
                    playerMovement.enabled = true;
                    GetComponent<PlotInfo>().UpdatePlotInfo();
                }
                else
                {
                    //returned true, so display inventory
                    if (backpack.transform.localScale.x < 1.0f)
                    {
                        backpack.StopClosing();
                        StartCoroutine(ShowBackpack());
                    }
                }
            }
        }*/
    }

    private IEnumerator ShowBackpack()
    {
        backpack.ShowTextBox();
        yield return new WaitUntil(() => backpack.transform.localScale.x >= 1.0f);
        this.enabled = false;
    }

    public void StopWaiting()
    {
        wait = false;
        choosing = false;
        this.enabled = true;
        GetComponent<PlotInfo>().UpdatePlotInfo();
    }

    public void ReceiveVegetable(string vegeName, int amount, string description)
    {
        InventoryItem vegetable = new InventoryItem(vegeName, description, backpack.GetVegetableSprite(vegeName), amount);
        backpack.AddItem(vegetable);
    }

    public void StartChoosing()
    {
        choosing = true;
    }

    public void CanNowMove()
    {
        Debug.Log("InventoryInteraction's CanNowMove activating movement");
        playerMovement.ActivateMovement();
        GetComponent<PlotInfo>().UpdatePlotInfo();
    }

    public void StopUsingFridge(bool allowMovement)
    {
        usingFridge = false;
        talkToNpc.FinishUsingFridge(allowMovement);
    }
}
