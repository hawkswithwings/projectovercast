﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InfoForPlot
{
    public float wateredPercentage;
    public float amountWatered;
    public bool hasVege;
    public InfoForVeggie veggie;

    public int textIndex;

    public InfoForPlot()
    {
        wateredPercentage = 0.0f;
        amountWatered = 0.0f;
        hasVege = false;
        veggie = new InfoForVeggie();
        textIndex = 0;
    }

    public void SetInfo(Plot other)
    {
        wateredPercentage = other.wateredPercentage;
        amountWatered = other.amountWatered;
        textIndex = other.textIndex;
        hasVege = other.hasVege;
        if (hasVege)
            veggie.SetInfo(other.vege);
        else
            veggie = new InfoForVeggie();
    }

}

[System.Serializable]
public class InfoForVeggie
{
    public string vegeName;
    public int dayPlanted;
    public float daysSincePlanted;
    public float daysToGrow;
    public float waterNeededDaily;
    public float currentWater;
    public float percentGrowth;
    public string effect;
    public int AmountYielded;

    public string description;

    public InfoForVeggie()
    {
        vegeName = "name";
        dayPlanted = 0;
        daysSincePlanted = 0;
        daysToGrow = 0;
        waterNeededDaily = 0;
        currentWater = 0;
        percentGrowth = 0;
        effect = "";
        description = "";
    }

    public void SetInfo(Vegetable other)
    {
        vegeName = other.vegeName;
        dayPlanted = other.dayPlanted;
        daysSincePlanted = other.daysSincePlanted;
        daysToGrow = other.daysToGrow;
        waterNeededDaily = other.waterNeededDaily;
        currentWater = other.currentWater;
        effect = other.effect;
        AmountYielded = other.AmountYielded;
        description = other.description;
    }
}
