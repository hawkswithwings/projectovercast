﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vegetable : MonoBehaviour
{
    public string vegeName = "name";
    public int dayPlanted = 0;
    public float daysSincePlanted;
    public float daysToGrow;
    public float waterNeededDaily;
    public float currentWater = 0;
    public float percentGrowth;
    public string effect;
    public int AmountYielded;

    public string description;

    
    // Start is called before the first frame update
    void Start()
    {
        daysToGrow = 5;
        waterNeededDaily = 100;
        //daysSincePlanted = 0;
        //percentGrowth = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //percentGrowth = ((daysSincePlanted / daysToGrow) * 100);
    }

    public void WaterPlant(int amount)
    {
        currentWater += amount;
        if (currentWater < waterNeededDaily)
        {
            //Need to print that the plant is watered enough today
        }
    }

    public void Empty()
    {
        vegeName = "name";
        dayPlanted = 0;
        daysSincePlanted = 0;
        daysToGrow = 0;
        waterNeededDaily = 0;
        currentWater = 0;
        percentGrowth = 0;
        effect = "";
        description = "";
    }

    public void SetToOther(InfoForVeggie other)
    {
        vegeName = other.vegeName;
        dayPlanted = other.dayPlanted;
        daysSincePlanted = other.daysSincePlanted;
        daysToGrow = other.daysToGrow;
        waterNeededDaily = other.waterNeededDaily;
        currentWater = other.currentWater;
        effect = other.effect;
        AmountYielded = other.AmountYielded;
        description = other.description;
    }

}
