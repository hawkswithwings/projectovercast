﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlotInfo : MonoBehaviour
{
    public List<InfoForPlot> plots;

    private void Start()
    {
        plots = new List<InfoForPlot>();
        for(int i = 0; i < 4; i++)
        {
            plots.Add(new InfoForPlot());
        }
    }


    //Called once the player finishes interacting with a plot, saves plots into list of plots and veggies
    public void UpdatePlotInfo()
    {
        GameObject[] plotsInScene = GameObject.FindGameObjectsWithTag("FarmPlot");
        for (int i = 0; i < plotsInScene.Length; i++)
        {
            plots[i].SetInfo(plotsInScene[i].GetComponent<Plot>());
        }
    }

    //Called when the player enters the scene with plots
    public void LoadPlotInfo()
    {
        GameObject[] plotsInScene = GameObject.FindGameObjectsWithTag("FarmPlot");
        for (int i = 0; i < plotsInScene.Length; i++)
        {
            plotsInScene[i].GetComponent<Plot>().SetToOther(plots[i]);
        }
        
    }
}
