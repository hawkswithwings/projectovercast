﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;


public class Plot : MonoBehaviour
{
    public DialogueRunner runner;
    public YarnProgram scriptToLoad;
    public bool addYarnScript;
    private YarnVariableStorage variableStorage;
    private ModifiedDialogueUI dialogueUI;

    private DialogueBox messageBox;
    private DialogueOptions yesOrNo;
    private string[] text = new string[4];
    public int textIndex;
    private bool boxVisible;


    public Vegetable vege;
    public float wateredPercentage;
    public float amountWatered;
    public bool hasVege;

    private bool waitForInventory;
    private string aOrAn;


    private bool doNotStart = false;
    private bool watered;
    public void SetToOther(InfoForPlot other)
    {
        if (!vege.name.Equals("name"))
        {
            return;
        }

        wateredPercentage = other.wateredPercentage;
        amountWatered = other.amountWatered;
        hasVege = other.hasVege;
        textIndex = other.textIndex;
        if (hasVege)
        {
            vege.SetToOther(other.veggie);
            if (vege.daysSincePlanted >= vege.daysToGrow && vege.vegeName != "name")
                textIndex = 3;
            if ("AEIOUaeiou".IndexOf(vege.vegeName[0]) == -1)
                aOrAn = "a";
            else
                aOrAn = "an";
        }
        else
        {
            vege.Empty();
        }
        doNotStart = true;
    }
        
    // Start is called before the first frame update
    void Start()
    {
        messageBox = GameObject.FindGameObjectWithTag("TextBox").GetComponent<DialogueBox>();
        yesOrNo = GameObject.FindGameObjectWithTag("YesOrNo").GetComponent<DialogueOptions>();

        text[0] = "Would you like to plant something?";
        text[1] = " here. Do you want to water it?"; //Add on to this when planted.
        text[2] = "The soil is moist and healthy from your watering.";
        text[3] = "Do you want to harvest your ";

        if (!doNotStart)
        {
            textIndex = 0;
            if (vege.daysSincePlanted >= vege.daysToGrow && vege.vegeName != "name")
                textIndex = 3;

            boxVisible = false;

            amountWatered = 0;
            wateredPercentage = 0;
            amountWatered = 0;
            waitForInventory = false;
            aOrAn = "a";
        }


        runner = FindObjectOfType<DialogueRunner>();
        variableStorage = (YarnVariableStorage)runner.variableStorage;
        dialogueUI = runner.gameObject.GetComponent<ModifiedDialogueUI>();

        watered = false;
    }

    // Update is called once per frame
    void Update()
    {
        //wateredPercentage = (vege.waterNeededDaily/amountWatered) * 100;
    }

    [YarnCommand("addWater")]
    public void addWater(){
        vege.WaterPlant(10);
        if(vege.currentWater >= vege.waterNeededDaily)
        {
            vege.currentWater = vege.waterNeededDaily;
        }
        watered = true;
    }

    [YarnCommand("harvestVege")]
    public void harvestVege()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<InventoryInteraction>().ReceiveVegetable(vege.vegeName, vege.AmountYielded, vege.description);
        string vegeReceived = vege.AmountYielded + " ";
        if(vege.AmountYielded > 1)
        {
            switch (vege.vegeName)
            {
                //carrot, beets, broccoli, onion, and asparagus
                case "Asparagus":
                case "Broccoli":
                    vegeReceived += vege.vegeName;
                    break;
                default:
                    vegeReceived += vege.vegeName + "s";
                    break;
            }
        }
        else
            vegeReceived += vege.vegeName;
        vege.Empty();
        hasVege = false;
        variableStorage.SetValue("$vegeReceived", new Yarn.Value(vegeReceived));
    }

    public void getStats(){
        //Debug.Log(wateredPercentage);
    }

    public void OldDisplayMessage()
    {
        if(messageBox.transform.localScale.x < 1.0f && !boxVisible)
        {
            StartCoroutine(BringUpBox(messageBox));
        }
    }

    public void DisplayMessage()
    {
        if (runner.IsDialogueRunning)
        {
            dialogueUI.MarkLineComplete();
            return;
        }
        variableStorage.SetValue("$plotName", new Yarn.Value(gameObject.name));
        variableStorage.SetValue("$planted", new Yarn.Value(false));
        variableStorage.SetValue("$watered", new Yarn.Value(watered));
        variableStorage.SetValue("$ready", new Yarn.Value(false));
        if (vege.vegeName != "name")
        {
            string vegDisplayStart = "A ";
            if("aeiouAEIOU".IndexOf(vege.vegeName[0]) >= 0)
                vegDisplayStart = "An ";
            vegDisplayStart += vege.vegeName;

            variableStorage.SetValue("$VegDisplayStart", new Yarn.Value(vegDisplayStart));
            variableStorage.SetValue("$VegName", new Yarn.Value(vege.vegeName));

            variableStorage.SetValue("$planted", new Yarn.Value(true));
            if (vege.vegeName.Equals("Carrot"))
                Debug.Log(vege.percentGrowth);
            if (vege.percentGrowth >= 100)
                variableStorage.SetValue("$ready", new Yarn.Value(true));
            else
                variableStorage.SetValue("$ready", new Yarn.Value(false));
        }

        runner.StartDialogue(scriptToLoad.name);
    }

    public bool DisplayOptions()
    {
        if (messageBox.DisplayNextLine() == false)
        {
            //Bring up the yes or no box
            if(textIndex != 2)
            {
                StartCoroutine(BringUpBox(yesOrNo));
            }
            else
            {
                CloseBoxes();
                boxVisible = false;
            }
            return true;
        }
        else
        {
            return false;
        }

    }

    public bool PerformSelectedAction()
    {
        if(textIndex != 2)
        {
            int plant = yesOrNo.GetSelectedOption();
            if (plant == 0)
            {
                //if textIndex = 0, plant a vegetable
                if (textIndex == 0)
                {
                    //Needs to pull up inventory and select the vegetable to plant
                    if (!waitForInventory)
                    {
                        CloseBoxes();
                        waitForInventory = true;
                    }
                    return true;
                }
                else if (textIndex == 1)
                {
                    vege.WaterPlant(10);
                    CloseBoxes();
                    if (vege.currentWater >= vege.waterNeededDaily)
                    {
                        vege.currentWater = vege.waterNeededDaily;
                    }
                    textIndex = 2;
                    return false;
                }
                else
                {
                    //Clear out vegetable, give it to player
                    GameObject.FindGameObjectWithTag("Player").GetComponent<InventoryInteraction>().ReceiveVegetable(vege.vegeName, vege.AmountYielded, vege.description);
                    vege.Empty();
                    textIndex = 0;
                    hasVege = false;
                    CloseBoxes();
                    return false;
                }
            }
            else
            {
                //close everything if textIndex is 1, otherwise a bit more for 0
                CloseBoxes();
                return false;
            }
        }
        else
        {
            //CloseBoxes();
            return false;
        }
        
    }

    private void CloseBoxes()
    {
        yesOrNo.ResetCursor();
        StartCoroutine(CloseBox(yesOrNo));
        StartCoroutine(CloseBox(messageBox));
        boxVisible = false;
    }

    public IEnumerator BringUpBox(TextBoxActions tba)
    {
        tba.ShowTextBox();
        
        yield return new WaitUntil(() => tba.transform.localScale.x >= 1.0f);
        try
        {
            if (textIndex == 0)
                ((DialogueBox)tba).SetTextDisplay(text[textIndex]);
            else if (textIndex == 1)
                ((DialogueBox)tba).SetTextDisplay("You planted "+aOrAn + " " + vege.vegeName + text[textIndex]);
            else if (textIndex == 2)
            {
                ((DialogueBox)tba).SetTextDisplay(text[textIndex]);
            }
            else if(textIndex == 3)
            {
                ((DialogueBox)tba).SetTextDisplay(text[textIndex] + vege.vegeName + "?");
            }
            ((DialogueBox)tba).StartTalking();
        }
        catch (Exception e) { }
            
        boxVisible = true;
    }

    public IEnumerator CloseBox(TextBoxActions tba)
    {
        tba.HideTextBox();
        yield return new WaitUntil(() => tba.transform.localScale.x <= 0.0f);
    }

    public void PlantVegetable(string vegeName, string description)
    {
        vege.vegeName = vegeName;
        vege.description = description;
        hasVege = true;
        switch (vegeName)
        {
            case "Carrot":
                aOrAn = "a";
                break;
            case "Turnip":
                aOrAn = "a";
                break;
            case "Beet":
                aOrAn = "a";
                break;
            case "Asparagus":
                aOrAn = "an";
                break;
            case "Cauliflower":
                aOrAn = "a";
                break;
            case "Onion":
                aOrAn = "an";
                break;
        }
    }

    public void StopWaitingForInventory()
    {
        waitForInventory = false;
        if(vege.vegeName != "name")
            textIndex = 1;
    }

    public bool BoxesDisplayed()
    {
        return boxVisible;
    }

    public void ChooseOption(int choice)
    {
        dialogueUI.SelectOption(choice);
    }
}
