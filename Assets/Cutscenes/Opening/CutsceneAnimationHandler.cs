﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CutsceneAnimationHandler : MonoBehaviour
{
    public bool setCamera;
    private bool fix = false;
    public Animator pAnimator;
    public PlayerMovement playerMovement;
    private RuntimeAnimatorController pAnim;
    public PlayableDirector director;

    // Start is called before the first frame update
    void OnEnable()
    {
        pAnim = pAnimator.runtimeAnimatorController;
        pAnimator.runtimeAnimatorController = null;


        if (setCamera)
        {
            GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
            foreach(PlayableBinding asset in director.playableAsset.outputs)
            {
                if(asset.streamName.Equals("Cinemachine Track"))
                {
                    director.SetGenericBinding(asset.sourceObject, camera.GetComponent<Cinemachine.CinemachineBrain>());
                    break;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(director.state != PlayState.Playing && !fix)
        {
            pAnimator.runtimeAnimatorController = pAnim;
            fix = true;
            if(playerMovement != null)
                playerMovement.ActivateMovement();
        }
    }

    public void updatePlayableDirector(PlayableDirector director)
    {
        this.director = director;
    }

    public void playCutscene()
    {
        fix = false;
        director.Play();
    }
}
