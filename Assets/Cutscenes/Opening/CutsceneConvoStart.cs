﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Yarn.Unity;
using UnityEngine.SceneManagement;

public class CutsceneConvoStart : MonoBehaviour
{
    public PlayableDirector director;
    public TalkingToYourselfTemp convo;
    public YarnProgram fileToRun;

    public string sceneToLoad;
    public int maxActivationCount;

    private int activationCount = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnEnable()
    {
        if (!string.IsNullOrEmpty(sceneToLoad))
        {
            activationCount++;
            if (activationCount >= maxActivationCount)
            {
                SceneManager.LoadScene(sceneToLoad);
                return;
            }
        }

        StartDialogue();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartDialogue()
    {
        director.playableGraph.GetRootPlayable(0).Pause();
        convo.cutsceneConvoStart = this;
        //if(fileToRun != null)
            //convo.UpdateYarnFile(fileToRun);
        convo.enabled = true;
        convo.StartTalking();
    }

    public void ResumeCutscene()
    {
        director.playableGraph.GetRootPlayable(0).Play();
    }

}
